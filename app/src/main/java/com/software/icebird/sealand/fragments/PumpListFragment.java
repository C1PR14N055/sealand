package com.software.icebird.sealand.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.utils.Constants;

/**
 * Created by Catalin on 24.09.2016.
 */

public class PumpListFragment extends Fragment {

    private RecyclerView recyclerView_pump;
    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mContext = Constants.APP_CONTEXT;

        View itemView = inflater.inflate(R.layout.fragment_pump_list, container, false);

        recyclerView_pump = (RecyclerView) itemView.findViewById(R.id.recyclerView_fragmentPumpList_pump);
        recyclerView_pump.setNestedScrollingEnabled(false);
        recyclerView_pump.setAdapter(Constants.pumpAdapter);
        recyclerView_pump.setLayoutManager(new GridLayoutManager(mContext, 4));

        return itemView;

    }

}
