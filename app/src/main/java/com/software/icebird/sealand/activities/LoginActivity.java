package com.software.icebird.sealand.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;

public class LoginActivity extends AppCompatActivity {

    private ProgressDialog mLoadingProgressDialog;
    private boolean isLoginWithSuccess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //refresh application context in constants to have access at it in other class
        Constants.APP_CONTEXT = LoginActivity.this;
        init();
    }

    private void init() {
        //init Context in Constanst
        Constants.APP_CONTEXT = LoginActivity.this;

        Log.d("Init", "true");
        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText usernameEditText = (EditText) findViewById(R.id.editText_login_inputUsername);
                EditText passwordEditText = (EditText) findViewById(R.id.editText_login_inputPassword);

                String username = Functions.escapeSQL(usernameEditText.getText().toString());
                String password = passwordEditText.getText().toString();

                if (usernameEditText.getText().toString().equals("")
                        || password.equals("")) {
                    dialogNotify(getString(R.string.loginActivity_dialog_title_warning), getString(R.string.loginActivity_dialog_emptyFields_message));
                } else {
                    loadingProgressBar();
                    String resEmail = Functions.execSQLGetFirstString("SELECT * FROM Member WHERE Email == " +
                            username + ";", "Email");
                    if (resEmail == null) {

                        isLoginWithSuccess = true;


                        dialogNotify(getString(R.string.loginActivity_dialog_title_warning), getString(R.string.loginActivity_dialog_incorrectCredential_message));
                    } else {

                        try{
                            String originalHash = Functions.execSQLGetFirstString("SELECT * FROM Member WHERE Email == " +
                                    username + ";", "Password");
                            String originalSalt = Functions.execSQLGetFirstString("SELECT * FROM Member WHERE Email == " +
                                    username + ";", "Salt");

                            Log.d("password", password);
                            Log.d("originalSalt", originalSalt);
                            Log.d("originalHash", originalHash);
                            Log.d("hashedHash", Functions.hashPassword(password, originalSalt).substring(3));


                            if (Functions.hashPasswordAssertEqualsHash(originalHash,
                                    password, originalSalt)) {
                                Functions.writeStringToPrefs(Constants.APP_CONTEXT,
                                        Constants.PREF_LOGGED_IN_MEMBER_EMAIL, username);

                                String userFirstName = Functions.execSQLGetFirstString("SELECT * FROM Member WHERE Email == " + username +";","FirstName");
                                String userSurname = Functions.execSQLGetFirstString("SELECT * FROM Member WHERE Email == " + username +";","Surname");

                                if(userFirstName == null) {
                                    userFirstName = "";
                                }

                                if(userSurname == null) {
                                    userSurname = "";
                                }

                                StringBuilder user = new StringBuilder();
                                user.append(userFirstName).append(" ").append(userSurname);

                                Functions.writeStringToPrefs(LoginActivity.this, "user", user.toString());

                                isLoginWithSuccess = true;

                                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                isLoginWithSuccess = true;

                                dialogNotify(getString(R.string.loginActivity_dialog_title_warning), getString(R.string.loginActivity_dialog_incorrectCredential_message));
                            }
                        } catch (Exception e) {
                            Log.e("LOGIN_BTN", e.getMessage());
                        }
                    }
                }
            }
        });
    }

    private void dialogNotify(String title, String text) {
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle(title)
                .setMessage(text)
                .setIcon(android.R.drawable.stat_sys_warning)
                .setPositiveButton(getString(R.string.okButton), null)
                .show();
    }

    private void loadingProgressBar() {
        mLoadingProgressDialog=new ProgressDialog(this);
        mLoadingProgressDialog.setMessage(getString(R.string.loginActivity_dialog_loadingLogin_message));
        mLoadingProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mLoadingProgressDialog.setIndeterminate(true);
        mLoadingProgressDialog.show();

        final Thread t = new Thread() {
            @Override
            public void run() {
                while(!isLoginWithSuccess) {
                    try {
                        sleep(200);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                mLoadingProgressDialog.dismiss();
            }
        };
        t.start();
    }
}
