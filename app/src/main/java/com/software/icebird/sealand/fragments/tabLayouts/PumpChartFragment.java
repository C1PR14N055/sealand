package com.software.icebird.sealand.fragments.tabLayouts;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.software.icebird.sealand.R;

/**
 * Created by ch on 08.09.2016.
 */
public class PumpChartFragment extends Fragment {

    private static View mItemView;
    private WebView mWebView;
    private ProgressBar mLoadingChart;
    private TextView mPumpNameTextView, mPumpCodeTextView;
    private String mUrl = "", mPumpName, mPumpCode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mItemView = inflater.inflate(R.layout.fragment_pump_chart, container, false);
        mLoadingChart = (ProgressBar) mItemView.findViewById(R.id.progressBar_fragmentChart_loadingChart);
        loadUrl();
        createTextField();

        return mItemView;
    }

    public void setUrlToLoad(String url) {
        mUrl = url;
    }

    public void loadUrl() {
        if(mItemView == null) {
            Log.e("ITEMVIEW CHART", "NULL");
            return;
        }

        mWebView = (WebView) mItemView.findViewById(R.id.fragment_pump_chart_web_view);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mWebView.setVisibility(View.INVISIBLE);
                mLoadingChart.setVisibility(View.VISIBLE);

                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mLoadingChart.setVisibility(View.INVISIBLE);
                mWebView.setVisibility(View.VISIBLE);
                super.onPageFinished(view, url);
            }
        });

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(mUrl);
        Log.d("CHART URL", mUrl);
    }

    public void createTextField() {
        if(mItemView == null) {
            return;
        }

        mPumpNameTextView = (TextView) mItemView.findViewById(R.id.textView_fragmentChart_pumpName);
        mPumpNameTextView.setText(mPumpName);

        mPumpCodeTextView = (TextView) mItemView.findViewById(R.id.textView_fragmentChart_pumpCode);
        mPumpCodeTextView.setText(mPumpCode);
    }

    public void setPumpInfo(String name, String code) {
        mPumpName = name;
        mPumpCode = code;

        if(mPumpNameTextView != null && mPumpCodeTextView != null) {
            mPumpNameTextView.setText(name);
            mPumpCodeTextView.setText(code);
        }
    }

}