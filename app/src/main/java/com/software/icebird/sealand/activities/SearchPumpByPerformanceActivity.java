package com.software.icebird.sealand.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.software.icebird.sealand.MenuManager;
import com.software.icebird.sealand.Pump;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.adapters.PumpAdapter;
import com.software.icebird.sealand.adapters.TextSpinnerAdapter;
import com.software.icebird.sealand.fragments.PumpDetailsFragment;
import com.software.icebird.sealand.fragments.PumpListFragment;
import com.software.icebird.sealand.interfaces.OnClickListenerRecyclerView;
import com.software.icebird.sealand.interfaces.OnRefreshActivity;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;
import com.software.icebird.sealand.utils.LocaleLanguageChanger;
import com.software.icebird.sealand.utils.ProgressDialogCreator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchPumpByPerformanceActivity extends AppCompatActivity implements OnClickListenerRecyclerView, OnRefreshActivity {

    private Context mContext;
    //private RecyclerView mPumpRecyclerView;
    private PumpAdapter mPumpAdapter;
    private List<Pump> mMatchingPumps;
    private ProgressDialogCreator mLoadingProgressDialog;

    //Fragments
    private PumpListFragment mPumpListFragment;
    private PumpDetailsFragment mPumpDetailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_pump_by_performance);

        mContext = this;

        init();
    }

    private void init() {

        mContext = SearchPumpByPerformanceActivity.this;

        //init Context in Constanst
        Constants.APP_CONTEXT = SearchPumpByPerformanceActivity.this;

        LinearLayout linearLayout_appMenu = (LinearLayout) findViewById(R.id.linearLayout_searchPumpByPerformance_appMenu);
        MenuManager menuManager = new MenuManager(mContext, linearLayout_appMenu, this);
        menuManager.initializeComponents();

        //init freq spinner
        final Spinner spinnerFreq = (Spinner) findViewById(R.id.spinner_searchPumpByPerformance_frequency);
        TextSpinnerAdapter adapterFreq = new TextSpinnerAdapter(mContext, R.array.pump_freq);
        spinnerFreq.setAdapter(adapterFreq);

        //init application spinner
        ArrayList<String> list = new ArrayList<>();

        try{
            Cursor cursor = Functions.execSQLGetCursor("SELECT * FROM Application WHERE BrandID = 1 ORDER BY Sort DESC");
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);

                switch (LocaleLanguageChanger.getIndexLanguage()) {
                    case 0:{
                        list.add(cursor.getString(cursor.getColumnIndex("Title")));
                        break;
                    }

                    case 1: {
                        list.add(cursor.getString(cursor.getColumnIndex("Title__it_IT")));
                        break;
                    }

                    case 2: {
                        list.add(cursor.getString(cursor.getColumnIndex("Title__fr_FR")));
                        break;
                    }

                    case 3: {
                        list.add(cursor.getString(cursor.getColumnIndex("Title__es_ES")));
                        break;
                    }
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.e("SEARCH_PERF", e.getMessage());
        }

        final Spinner spinnerApplication = (Spinner) findViewById(R.id.spinner_searchPumpByPerformance_application);
        TextSpinnerAdapter adapterApplication = new TextSpinnerAdapter(mContext, list);

        spinnerApplication.setAdapter(adapterApplication);

        //head & flow input
        final EditText headEdiText = (EditText) findViewById(R.id.editText_searchPumpByPerformance_pumpHead);
        final EditText flowEdiText = (EditText) findViewById(R.id.editText_searchPumpByPerformance_pumpFlow);

        //head measure units
        final Spinner headUnitSpinner = (Spinner) findViewById(R.id.spinner_searchPumpByPerformance_pumpHeadUnit);
        TextSpinnerAdapter adapterHeadUnits = new TextSpinnerAdapter(mContext, R.array.pump_head_units);

        headUnitSpinner.setAdapter(adapterHeadUnits);

        //flow measure units
        final Spinner flowUnitSpinner = (Spinner) findViewById(R.id.spinner_searchPumpByPerformance_pumpFlowUnit);
        TextSpinnerAdapter adapterFlowUnits = new TextSpinnerAdapter(mContext, R.array.pump_flow_units);

        flowUnitSpinner.setAdapter(adapterFlowUnits);

        final Button searchButton = (Button) findViewById(R.id.button_searchPumpByPerformance_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLoadingProgressDialog = new ProgressDialogCreator(mContext, getString(R.string.searchBy_commonFields_progressDialog_searchingPumps_title),
                        getString(R.string.searchBy_commonFields_progressDialog_searchingPumps_message), true, ProgressDialog.STYLE_SPINNER);

                if(headEdiText.getText().toString().isEmpty() || flowEdiText.getText().toString().isEmpty()){
                    Toast.makeText(mContext, getString(R.string.searchByPerformance_dialog_completeAllFields_message), Toast.LENGTH_SHORT).show();
                } else {

                    View focusedView = getCurrentFocus();
                    if (focusedView != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    mLoadingProgressDialog.showDialog();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String freq = spinnerFreq.getSelectedItem().toString();
                            String appl = spinnerApplication.getSelectedItem().toString();


                            String headStr = headEdiText.getText().toString();
                            String flowStr = flowEdiText.getText().toString();

                            if((headStr.length() == 1 && (headStr.contains(".") || headStr.contains(",")))||
                                flowStr.length() == 1 && (flowStr.contains(".") || flowStr.contains(","))){

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mLoadingProgressDialog.dismissDialog();
                                        Functions.toast(mContext, "Please check the input values!", true);
                                    }
                                });
                            } else {
                                double head = Double.valueOf(headEdiText.getText().toString());
                                double flow = Double.valueOf(flowEdiText.getText().toString());

                                head = Math.abs(head);
                                flow = Math.abs(flow);
                                String headUnit = headUnitSpinner.getSelectedItem().toString();
                                String flowUnit = flowUnitSpinner.getSelectedItem().toString();

                                searchPumpsByPerformance(freq, appl, head, flow, headUnit, flowUnit);
                            }
                        }
                    }).start();
                }
            }
        });
    }

    private void searchPumpsByPerformance(String freq, String appl, double head, double flow,
                                          String headUnit, String flowUnit) {
        try{
            int frequency = freq.equals("50Hz") ? 50 : 60;
            ArrayList<Integer> applicationSeriesIDs;
            Cursor cursor;
            if (appl.equals(getString(R.string.item_spinner_allProduct))) {
                Log.d("selectedApplicationId", "all");
                applicationSeriesIDs = new ArrayList<>();
                cursor = Functions.execSQLGetCursor("SELECT * FROM Serie");
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    applicationSeriesIDs.add(cursor.getInt(cursor.getColumnIndex("ID")));
                }
            } else {
                String translatedTitle = "Title";
                switch (LocaleLanguageChanger.getIndexLanguage()) {
                    case 0:{
                        translatedTitle = "Title";
                        break;
                    }
                    case 1: {
                        translatedTitle = "Title__it_IT";
                        break;
                    }
                    case 2: {
                        translatedTitle = "Title__fr_FR";
                        break;
                    }
                    case 3: {
                        translatedTitle = "Title__es_ES";
                        break;
                    }
                }
                String selectedApplicationId = Functions.execSQLGetFirstString("SELECT * FROM Application WHERE " +
                        translatedTitle + " = " + Functions.escapeSQL(appl), "ID");
                Log.d("selectedApplicationId", selectedApplicationId);
                applicationSeriesIDs = new ArrayList<>();
                cursor= Functions.execSQLGetCursor("SELECT * FROM Application_Series WHERE ApplicationID = "
                        + selectedApplicationId);
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    applicationSeriesIDs.add(cursor.getInt(cursor.getColumnIndex("SerieID")));
                }
            }

            cursor.close();

            double originalFlow = flow;
            flow = Functions.convertFlow(flow, flowUnit);

            ArrayList<Integer> matchingFlowAndFrequencyPumpIDs = new ArrayList<>();

            for (int i : applicationSeriesIDs) {
                Log.d("Series ", String.valueOf(i));
                cursor = Functions.execSQLGetCursor("SELECT * FROM Pump WHERE SerieID = " + i +
                        " AND Frequency = " + frequency + " AND " + flow + " BETWEEN CAST(AppRangeMin AS FLOAT) AND" +
                        " CAST(AppRangeMax AS FLOAT) AND BrandID = " + Functions.getSelectedBrand(mContext));
                if (cursor.getCount() > 0) {
                    for (int j = 0; j < cursor.getCount(); j++) {
                        cursor.moveToPosition(j);
                        matchingFlowAndFrequencyPumpIDs.add(cursor.getInt(cursor.getColumnIndex("ID")));
                    }
                }
                cursor.close();
            }

            Log.d("flow ", String.valueOf(flow));
            Collections.sort(matchingFlowAndFrequencyPumpIDs);
            for (int i : matchingFlowAndFrequencyPumpIDs) {
                Log.d("PUMP >< APP MIN MAX ", String.valueOf(i));
            }

            mMatchingPumps = new ArrayList<>();
            for (int i : matchingFlowAndFrequencyPumpIDs) {
                cursor = Functions.execSQLGetCursor("SELECT * FROM Pump WHERE ID = " + i + " AND BrandID = " +
                        Functions.getSelectedBrand(mContext));
                cursor.moveToFirst();
                String equation = getEquation(i, "HEAD");

                // pump object vars set
                Pump pump = new Pump();
                pump.id = i;

                try {
                    pump.errorMax = Double.parseDouble(cursor.getString(cursor.getColumnIndex("ErrorMax")));
                    pump.errorMin = Double.parseDouble(cursor.getString(cursor.getColumnIndex("ErrorMin")));
                } catch (Exception eParse) {
                    eParse.printStackTrace();
                    Log.e("MAX", cursor.getString(cursor.getColumnIndex("ID")));
                }
                double y = solveY(flow, equation, headUnit);

                double minY = head * (1 - (pump.errorMin / 100));
                double maxY = head * (1 + (pump.errorMax / 100));

                if (y >= minY && y <= maxY) {

                    // get pump image
                    int pumpImageInPumpID = Functions.execSQLGetFirstInt("SELECT * FROM PumpImage WHERE OwnerID = " + i, "ImageID");
                    String thumbImageFilePath = Constants.APP_CONTEXT.getFilesDir().getPath() + "/" +
                            Constants.THUMBS_DIR + "/" + Functions.execSQLGetFirstString("SELECT * FROM File WHERE ID = " +
                            pumpImageInPumpID, "Title");
                    // set pump image only if y >= minY && y <= maxY for efficiency
                    pump.setPumpName(cursor.getString(cursor.getColumnIndex("Title")));
                    pump.setPumpImagePath(thumbImageFilePath);

                    equation = getEquation(pump.id, "ETA");
                    pump.efficiency = solveY(flow, equation, headUnit);
                    equation = getEquation(pump.id, "POWER");
                    pump.power = solveY(flow, equation, headUnit);
                    equation = getEquation(pump.id, "NPSH");
                    pump.npsh = solveY(flow, equation, headUnit);

                    pump.x = originalFlow;
                    pump.y = head;
                    pump.xUnit = flowUnit;
                    pump.yUnit = headUnit;

                    pump.foundX = flow;
                    pump.foundY = y;
                    pump.hasPerformanceValues = true;
                    mMatchingPumps.add(pump);

                    Log.e("-------------", "-----------------");
                    Log.d("PUMP NAME    ", String.valueOf(pump.getPumpName()));
                    Log.d("PUMP ID      ", String.valueOf(pump.id));
                    Log.d("PUMP EFF     ", String.valueOf(pump.efficiency));
                    Log.d("PUMP MINY    ", String.valueOf(minY));
                    Log.d("PUMP MINX    ", String.valueOf(maxY));
                    Log.d("PUMP X       ", String.valueOf(flow));
                    Log.d("PUMP Y       ", String.valueOf(y));
                    Log.d("PUMP EQ HEAD ", getEquation(pump.id, "HEAD") != null ? getEquation(pump.id, "HEAD") : "NULL");
                    Log.d("PUMP EQ ETA  ", getEquation(pump.id, "ETA") != null ? getEquation(pump.id, "HEAD") : "NULL");
                    Log.d("PUMP EQ NPSH ", getEquation(pump.id, "NPSH") != null ? getEquation(pump.id, "HEAD") : "NULL");
                    Log.d("PUMP EQ POWER", getEquation(pump.id, "POWER") != null ? getEquation(pump.id, "HEAD") : "NULL");

                }
            }

            Collections.sort(mMatchingPumps, new Comparator<Pump>() {
                @Override
                public int compare(Pump pump1, Pump pump2) {
                    return Double.compare(pump1.efficiency, pump2.efficiency);
                }
            });

            Collections.reverse(mMatchingPumps);

            //if pump list is 0
            if(mMatchingPumps.size() == 0) {

                try {
                    Thread.sleep(500);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            cleanPumpListFragment();
                            mLoadingProgressDialog.dismissDialog();
                            Functions.toast(mContext, getString(R.string.searchBy_noPumpFound), true);
                        }
                    });

                }catch (Exception eThread) {
                    Log.e("NOPUMP", eThread.getMessage());
                }

                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    createPumpListFragment(mMatchingPumps);
                }
            });

        } catch (Exception ex) {
            Log.e("SEARCH_PERF_SEARCH", ex.getMessage());
        }
    }

    private double solveY(double x, String strEquation, String conversionRate) {
        if (strEquation == null) { return -1; }
        strEquation = strEquation.replace(",", ".");
        List<Double> listEquation = new ArrayList<>();
        for (String eq : strEquation.split("\t")) {
            listEquation.add(Double.parseDouble(eq));
        }
        double y = 0;
        for (int i = 0; i < listEquation.size(); i++) {
            y += Math.pow(x, i) * listEquation.get(i);
        }
        if (conversionRate != null) {
            y = Functions.convertHead(y, conversionRate);
        }

        Log.i("Y", String.valueOf(y));
        return y;
    }

    private String getEquation(int pumpId, String diagramType) {
        if (diagramType == null || diagramType.equals(""))
            diagramType = "HEAD";
        String eq = Functions.execSQLGetFirstString("SELECT * FROM Diagram WHERE PumpID = " + pumpId
                + " AND Type = " + Functions.escapeSQL(diagramType), "Equation");
        return eq;
    }

    //Fragment Manager

    @Override
    public void OnClickItem(View view, int position) {
        createPumpDetailsFragment(position);
    }

    private void createPumpListFragment(List<Pump> matchingPumps) {
        mPumpAdapter = new PumpAdapter(mContext, matchingPumps, this, PumpAdapter.PUMP_IMAGE_WITH_TEXT);

        Constants.pumpAdapter = mPumpAdapter;

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if(mPumpDetailsFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpDetailsFragment).commit();
            mPumpDetailsFragment = null;
        }

        if(mPumpListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpListFragment).commit();
            Log.i("REMOVE", "Remove PumpListFragment");
        }

//        mPumpListFragment = new PumpListFragment();
//
//        transaction.add(R.id.linearLayout_searchPumpByPerformance_fragmentContainer, mPumpListFragment);
//        transaction.commit();

        if(matchingPumps.size() == 0) {
            Functions.toast(mContext, getString(R.string.searchBy_noPumpFound), true);
            cleanPumpListFragment();
            return;
        }

        createPumpDetailsFragment(0);

        mLoadingProgressDialog.dismissDialog();
    }

    private void createPumpDetailsFragment(int position) {

        Bundle sendBundle = new Bundle();
        sendBundle.putInt(PumpDetailsFragment.TAG_CREATED_FROM, PumpDetailsFragment.TAG_CREATED_FROM_PUMP);
        sendBundle.putInt("pumpPosition", position); //object position in adapter

        Log.i("CREATE", "Create PumpListFragment");

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if(mPumpListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpListFragment).commit();
            mPumpListFragment = null;
        }

        if(mPumpDetailsFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpDetailsFragment).commit();
            Log.i("REMOVE", "Remove PumpListFragment");
        }

        mPumpDetailsFragment = new PumpDetailsFragment();
        mPumpDetailsFragment.setArguments(sendBundle);

        transaction.add(R.id.linearLayout_searchPumpByPerformance_fragmentContainer, mPumpDetailsFragment);
        transaction.commit();
    }

    /**
     * Clean the fragment when not found pumps
     */
    private void cleanPumpListFragment(){

        if(mPumpListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpListFragment).commit();
        }

        if(mPumpDetailsFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpDetailsFragment).commit();
        }
    }

    @Override
    public void onRefreshActivity() {
        try{
            Intent refresh = new Intent(mContext, SearchPumpByPerformanceActivity.class);
            startActivity(refresh);
            finish();
        } catch (Throwable e) {

        }
    }
}