package com.software.icebird.sealand.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.software.icebird.sealand.R;

import java.util.List;

/**
 * Created by baumdev on 07/11/2016.
 */

public class ElementsListViewAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> itemList;
    private LayoutInflater layoutInflater;

    private int selectedPosition = -1;

    public ElementsListViewAdapter(Context context, List<String> itemList) {
        super(context, R.layout.item_series_element, itemList);

        this.context = context;
        this.itemList = itemList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String pumpName = getItem(position);

        SeriesElementPumpViewHolder seriesElementPumpViewHolder;

        final View result;

        if(convertView == null) {
            seriesElementPumpViewHolder = new SeriesElementPumpViewHolder();

            convertView = layoutInflater.inflate(R.layout.item_series_element, parent, false);

            seriesElementPumpViewHolder.pumpName = (TextView) convertView.findViewById(R.id.textView_seriesElement_name);

            result = convertView;

            convertView.setTag(seriesElementPumpViewHolder);
        } else {
            seriesElementPumpViewHolder = (SeriesElementPumpViewHolder) convertView.getTag();
            result = convertView;
        }

        seriesElementPumpViewHolder.pumpName.setText(pumpName);

        if(selectedPosition == position) {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.blue_sealand));
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        return convertView;
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
    }

    private static class SeriesElementPumpViewHolder {
        TextView pumpName;
    }
}
