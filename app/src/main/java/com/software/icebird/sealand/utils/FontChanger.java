package com.software.icebird.sealand.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.lang.reflect.Field;

/**
 * Created by ch on 05.10.2016.
 */

public class FontChanger {

    public static void overrideFont(Context context, String defaultFontName, String customFontFileNameInAssets) {
        try{

            Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

            Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontName);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);

            Log.i("FONT_EXCEPTION", "Changed");

        } catch (Exception e) {
            Log.e("FONT_EXCEPTION","Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontName);
            Log.e("FONT_EXCEPTION", e.getMessage());
        }
    }
}
