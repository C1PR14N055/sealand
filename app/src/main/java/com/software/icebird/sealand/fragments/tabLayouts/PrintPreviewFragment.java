package com.software.icebird.sealand.fragments.tabLayouts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.utils.Constants;

import java.io.InputStream;

/**
 * Created by ch on 08.09.2016.
 */
public class PrintPreviewFragment extends Fragment {

    Context mContext;

    private View mItemView;
    private WebView mWebView;
    private ProgressBar mLoadingPDF;
    private TextView mPumpNameTextView, mPumpCodeTextView;
    private static String url = "", initURL = "";
    private String mPumpName, mPumpCode;

    private static boolean isLoaded = false, isFinished = false, isOverride = false;

    private static int loadedPumpId;
    private static String googlePdfURL = "";

    private static Thread checkLoadingThread;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        checkLoadingThread = null;

        isFinished = false;
        isLoaded = false;
        isOverride = false;
    }

    @Override
    public void onDetach() {

        super.onDetach();

        isLoaded = false;
        isOverride = false;
        isFinished = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mItemView = inflater.inflate(R.layout.fragment_print_preview, container, false);
        mLoadingPDF = (ProgressBar) mItemView.findViewById(R.id.progressBar_fragmentPrintPreview_loadingPDF);
        loadUrl();
        createTextField();
        return mItemView;
    }

    public void setUrlToLoad(String purl, int pumpId) {

        if(pumpId != loadedPumpId) {

            url = purl;
            initURL = purl;

            googlePdfURL = "";

            loadedPumpId = pumpId;
        }

        isLoaded = false;
        isOverride = false;
        isFinished = false;
    }

    public void loadUrl() {
        if(mItemView == null)
            return;


        mWebView = (WebView) mItemView.findViewById(R.id.fragment_print_preview_web_view);
        mWebView.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mWebView.setVisibility(View.INVISIBLE);
                mLoadingPDF.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String purl) {

                if (purl.endsWith(".pdf")) {

                    if(googlePdfURL.equals("")) {

                        url = Constants.GOOGLE_PDF_VIEWER_API + purl;

                        mWebView.loadUrl(url);

                        googlePdfURL = url;

                        Log.d("LOADING GOOGLE PDF URL", url);

                        isOverride = true;

                    } else {
                        isOverride = true;
                    }
                }
                return super.shouldOverrideUrlLoading(view, purl);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
//                mLoadingPDF.setVisibility(View.INVISIBLE);
                //mWebView.setVisibility(View.VISIBLE);

                super.onPageFinished(view, url);

                if(isOverride) {
                    isFinished = true;
                }
                injectCSS(".ndfHFb-c4YZDc-Wrql6b{display:none;}");
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Log.e("WEBVIEW", "ERROR");
                isLoaded = true;
                mLoadingPDF.setVisibility(View.INVISIBLE);
                mWebView.setVisibility(View.VISIBLE);
                super.onReceivedError(view, request, error);
            }
        });

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);

        if(googlePdfURL.equals("")){
            Log.i("URL", "GENERATE");
            mWebView.loadUrl(initURL);
        } else {
            Log.i("URL", "GET: " + googlePdfURL);
          mWebView.loadUrl(googlePdfURL);
        }


        if(checkLoadingThread == null) {
            checkLoadingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.i("Thread start", "start");

                    do{
                        if(isFinished && !isLoaded) {
                            Log.i("PDF", "UNLOADED");

                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            if(!isLoaded) {
                                ((Activity)mContext).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.i("RELOAD", url);
                                        isFinished = false;
                                        isOverride = false;
                                        mLoadingPDF.setVisibility(View.INVISIBLE);

                                        if(googlePdfURL.equals("")) {
                                            mWebView.loadUrl(initURL);
                                        } else {
                                            mWebView.loadUrl(googlePdfURL);
                                        }
                                    }
                                });
                            }

                        }

                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }while(!isLoaded);

                    checkLoadingThread = null;

                    Log.i("THREAD FINISH", "FINISH");
                }
            });

            checkLoadingThread.start();
        } else {
            checkLoadingThread = null;
        }
    }

    // Append css style to document head
    private void injectCSS(String css) {
        try {
            String encoded = Base64.encodeToString(css.getBytes(), Base64.NO_WRAP);
            mWebView.loadUrl("javascript:(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var style = document.createElement('style');" +
                    "style.type = 'text/css';" +
                    "style.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(style)" +
                    "})()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createTextField() {
        if(mItemView == null) {
            return;
        }

        mPumpNameTextView = (TextView) mItemView.findViewById(R.id.textView_fragmentPrint_pumpName);
        mPumpNameTextView.setText(mPumpName);

        mPumpCodeTextView = (TextView) mItemView.findViewById(R.id.textView_fragmentPrint_pumpCode);
        mPumpCodeTextView.setText(mPumpCode);
    }

    public void setPumpInfo(String name, String code) {
        mPumpName = name;
        mPumpCode = code;

        if(mPumpNameTextView != null && mPumpCodeTextView != null) {
            mPumpNameTextView.setText(name);
            mPumpCodeTextView.setText(code);
        }
    }
}
