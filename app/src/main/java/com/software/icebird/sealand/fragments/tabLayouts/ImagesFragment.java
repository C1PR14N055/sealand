package com.software.icebird.sealand.fragments.tabLayouts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.adapters.PumpImageAdapter;
import com.software.icebird.sealand.interfaces.OnClickListenerRecyclerView;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ch on 08.09.2016.
 */
public class ImagesFragment extends Fragment implements OnClickListenerRecyclerView{

    private List<String> mPathList;
    private RecyclerView mImageRecyclerView;
    private ProgressBar mLoadingProgressBar;
    private TextView mNamePumpTextView;
    private String mNamePump = "";
    private TextView mCodePumpTextView;
    private String mCodePump = "";

    private Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View itemView = inflater.inflate(R.layout.fragment_images, container, false);

        mImageRecyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerView_fragmentImages_pumpImages);
        setImageAdapter(new ArrayList<String>());
        mLoadingProgressBar = (ProgressBar) itemView.findViewById(R.id.progressBar_fragmentImages_loadingImage);

        mNamePumpTextView = (TextView) itemView.findViewById(R.id.textView_fragmentImages_pumpName);
        mNamePumpTextView.setText(mNamePump);
        mCodePumpTextView = (TextView) itemView.findViewById(R.id.textView_fragmentImages_pumpCode);
        mCodePumpTextView.setText(mCodePump);

        return itemView;
    }

    @Override
    public void OnClickItem(View view, int position) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = FileProvider.getUriForFile(Constants.APP_CONTEXT, "com.software.icebird.sealand",
                new File(mPathList.get(position)));
        //Functions.toast(mContext, String.valueOf(uri), true);
        String fullImageUri = uri.toString().replace("/" + Constants.THUMBS_DIR_NAME, "");
        intent.setDataAndType(Uri.parse(fullImageUri), "image/*");
        Log.d("IMAGE URI", fullImageUri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    public void getImageFromDatabase(final int pumpID, Context context) {

        mContext = context;

        mPathList = new ArrayList<>();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Cursor cursor = Functions.execSQLGetCursor("SELECT * FROM PumpImage WHERE OwnerID == '" + pumpID + "';");

                    ArrayList<Integer> idList = new ArrayList<>();

                    for(int i = 0; i < cursor.getCount(); i++) {
                        cursor.moveToPosition(i);

                        int imageIDIndex = cursor.getColumnIndex("ImageID");
                        int imageID = cursor.getInt(imageIDIndex);

                        if(!idList.contains(imageID)) {
                            idList.add(imageID);

                            // DO NOT change Title ti Title_IT on lang change
                            String path = Functions.execSQLGetFirstString(
                                    "SELECT * FROM File WHERE ClassName == 'Image' AND ID == '" + imageID + "';", "Title");

                            StringBuilder sb = new StringBuilder();

                            sb.append(Constants.APP_CONTEXT.getFilesDir());
                            sb.append("/");
                            sb.append(Constants.THUMBS_DIR);
                            sb.append("/");
                            sb.append(path);

                            Log.d("IMG PATH", sb.toString());

                            mPathList.add(sb.toString());
                        }
                    }

                    ((Activity)mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(mLoadingProgressBar != null) {
                                mLoadingProgressBar.setVisibility(View.GONE);
                                mImageRecyclerView.setVisibility(View.VISIBLE);
                                setImageAdapter(mPathList);
                            }

                        }
                    });

                } catch (Exception e) {
                    Log.e("Error IMages Fragment", e.getMessage());
                }
            }
        }).start();



    }

    private void setImageAdapter(List<String> path) {

        if(mImageRecyclerView != null) {
            PumpImageAdapter pumpImageAdapter = new PumpImageAdapter(mContext, path, this);
            mImageRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
            mImageRecyclerView.setAdapter(pumpImageAdapter);
        } else {
            Log.i("null", "recycler");
            return;
        }
    }

    public void setPumpInfo(String name, String code) {

        mNamePump = name;
        mCodePump = code;

        if(mCodePumpTextView != null && mNamePumpTextView != null) {
            mNamePumpTextView.setText(name);
            mCodePumpTextView.setText(code);
        } else {
            Log.i("query", "is null");
        }

    }

}
