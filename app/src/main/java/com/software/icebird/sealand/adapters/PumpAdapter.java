package com.software.icebird.sealand.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.software.icebird.sealand.Pump;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.adapters.viewHolders.PumpViewWithTextViewHolder;
import com.software.icebird.sealand.adapters.viewHolders.PumpViewWithoutTextViewHolder;
import com.software.icebird.sealand.interfaces.OnClickListenerRecyclerView;

import java.io.File;
import java.util.List;

/**
 * Created by catalin on 01/09/2016.
 */
public class PumpAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<Pump> mPumpList;

    private int mViewTypePump;

    private OnClickListenerRecyclerView mListenerRecyclerView;

    public static final int PUMP_IMAGE_WITH_TEXT = 0;
    public static final int PUMP_IMAGE_WITHOUT_TEXT = 1;

    public PumpAdapter(Context context, List<Pump> pumpList, OnClickListenerRecyclerView listenerRecyclerView, int viewHolderType) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(mContext);
        this.mPumpList = pumpList;
        mListenerRecyclerView = listenerRecyclerView;
        mViewTypePump = viewHolderType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (mViewTypePump) {
            case PUMP_IMAGE_WITH_TEXT: {
                View pumpView = mInflater.inflate(R.layout.pump_layout_with_text, parent, false);
                return new PumpViewWithTextViewHolder(pumpView, mListenerRecyclerView);
            }

            case PUMP_IMAGE_WITHOUT_TEXT:{
                View pumpView = mInflater.inflate(R.layout.pump_layout_without_text, parent, false);
                return new PumpViewWithoutTextViewHolder(pumpView, mListenerRecyclerView);
            }

            default:
                return null;
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof PumpViewWithTextViewHolder) {
            final PumpViewWithTextViewHolder pumpViewHolder = (PumpViewWithTextViewHolder) holder;
            final Pump p = mPumpList.get(position);
            pumpViewHolder.setPumpName(p.getPumpName());

            File imgFile = new File(p.getImageFilePath());
            if(imgFile.exists()){
                new Thread(new Runnable() {
                    public void run() {
                        final Bitmap bitmap =
                                BitmapFactory.decodeFile(p.getImageFilePath());
                        pumpViewHolder.mPumpImageView.post(new Runnable() {
                            public void run() {

                                pumpViewHolder.mPumpImageView.setImageBitmap(bitmap);
                                //stop loading image
                                pumpViewHolder.mPumpLoadingImage.setVisibility(View.GONE);

                                pumpViewHolder.mPumpImageView.refreshDrawableState();

                            }
                        });
                    }
                }).start();
            } else {
                pumpViewHolder.mPumpImageView.setImageResource(R.drawable.none_pump);
                pumpViewHolder.mPumpLoadingImage.setVisibility(View.GONE);
                pumpViewHolder.mPumpImageView.refreshDrawableState();
            }
        }

        if(holder instanceof PumpViewWithoutTextViewHolder) {
            final PumpViewWithTextViewHolder pumpViewHolder = (PumpViewWithTextViewHolder) holder;
            final Pump p = mPumpList.get(position);
            pumpViewHolder.setPumpName(p.getPumpName());

            File imgFile = new File(p.getImageFilePath());
            if (imgFile.exists()) {
                new Thread(new Runnable() {
                    public void run() {
                        final Bitmap bitmap =
                                BitmapFactory.decodeFile(p.getImageFilePath());
                        pumpViewHolder.mPumpImageView.post(new Runnable() {
                            public void run() {
                                //stop loading image
                                pumpViewHolder.mPumpImageView.setImageBitmap(bitmap);
                                pumpViewHolder.mPumpLoadingImage.setVisibility(View.GONE);
                                pumpViewHolder.mPumpImageView.refreshDrawableState();
                            }
                        });
                    }
                }).start();
            } else {
                //stop loading image
                pumpViewHolder.mPumpImageView.setImageResource(R.drawable.none_pump);
                pumpViewHolder.mPumpLoadingImage.setVisibility(View.GONE);
                pumpViewHolder.mPumpImageView.refreshDrawableState();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mPumpList.size();
    }

    /**
     *Set the View Type for Pump
     *      - 0 pump with textView
     *      - 1 pump without textView
     * @param type
     */
    public void setViewTypePump(int type){
        mViewTypePump = type;
    }

    public void addPumpToList(Pump pump) {
        mPumpList.add(pump);
    }

    public void changePumpList(List<Pump> pumpList) {
        mPumpList = pumpList;
    }

    public List<Pump> getPumpList(){
        return mPumpList;
    }

    public Pump getPumpAt(int position) {
        return mPumpList.get(position);
    }
}
