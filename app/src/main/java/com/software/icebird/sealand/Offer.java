package com.software.icebird.sealand;

import android.provider.Settings;

import com.software.icebird.sealand.utils.Constants;

/**
 * Created by C on 27/09.
 */

public class Offer {

    public String id;
    public String date;
    public int pumpID;
    public String pumpCode;
    public String pumpName;
    public String pumpChartUrl;
    public String pumpPdfPath;
    public String name;
    public String company;
    public String address;
    public String phone;
    public String fax;
    public String email;
    public String reference;
    public String pdfJsonID;
    public boolean synced = false;

    public Offer(){
        id = Settings.Secure.getString(Constants.APP_CONTEXT.getContentResolver(),
                Settings.Secure.ANDROID_ID) + System.currentTimeMillis();
        date = android.text.format.DateFormat.format("dd/MM/yyyy", new java.util.Date()).toString();
    }
}
