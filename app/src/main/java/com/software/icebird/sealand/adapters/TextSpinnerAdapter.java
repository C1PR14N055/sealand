package com.software.icebird.sealand.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.software.icebird.sealand.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ch on 23.09.2016.
 */

public class TextSpinnerAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private List<String> mItemsList;

    public TextSpinnerAdapter(Context context, ArrayList<String> itemsList) {
        super(context, R.layout.item_spinner_text, R.id.textView_spinnerText_text, itemsList);

        this.mContext = context;
        this.mItemsList = itemsList;
    }

    public TextSpinnerAdapter(Context context, int arrayFromResource) {
        super(context, R.layout.item_spinner_text, R.id.textView_spinnerText_text,
                Arrays.asList(context.getResources().getStringArray(arrayFromResource)));

        this.mContext = context;
        this.mItemsList = Arrays.asList(mContext.getResources().getStringArray(arrayFromResource));
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    public View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View item = inflater.inflate(R.layout.item_spinner_text, parent, false);

        TextView textView = (TextView) item.findViewById(R.id.textView_spinnerText_text);
        textView.setText(mItemsList.get(position));

        return item;
    }
}
