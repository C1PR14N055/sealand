package com.software.icebird.sealand.fragments.tabLayouts;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.software.icebird.sealand.Offer;
import com.software.icebird.sealand.Pump;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;
import com.software.icebird.sealand.utils.LocaleLanguageChanger;


/**
 * Created by ch on 13.09.2016.
 */
public class OfflineOffersFragment extends Fragment {

    private TextView mNamePumpTextView;
    private String mNamePump = "";
    private TextView mCodePumpTextView;
    private String mCodePump = "";
    private Pump mPump;

    ScrollView scrollView;
    EditText company;
    EditText name;
    EditText address;
    EditText phone;
    EditText fax;
    EditText email;
    EditText reference;
    Button createBtn;
    View scrollingView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void init() {

        mNamePumpTextView = (TextView) getView().findViewById(R.id.textView_fragmentOfflineOffers_pumpName);
        mNamePumpTextView.setText(mNamePump);
        mCodePumpTextView = (TextView) getView().findViewById(R.id.textView_fragmentOfflineOffers_pumpCode);
        mCodePumpTextView.setText(mCodePump);

        scrollView = (ScrollView) getView().findViewById(R.id.scrollView_fragmentOfflineOffers);
        company = (EditText) getView().findViewById(R.id.editText_fragmentOfflineOffers_company);
        name = (EditText) getView().findViewById(R.id.editText_fragmentOfflineOffers_name);
        address = (EditText) getView().findViewById(R.id.editText_fragmentOfflineOffers_address);
        phone = (EditText) getView().findViewById(R.id.editText_fragmentOfflineOffers_phone);
        fax = (EditText) getView().findViewById(R.id.editText_fragmentOfflineOffers_fax);
        email = (EditText) getView().findViewById(R.id.editText_fragmentOfflineOffers_email);
        reference = (EditText) getView().findViewById(R.id.editText_fragmentOfflineOffers_reference);
        createBtn = (Button) getView().findViewById(R.id.button_fragmentOfflineOffers_createOffer);
        scrollingView = (View) getView().findViewById(R.id.view_fragmentOfflineOffers_scrolling);

        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createOffer();
                hideKeyboard();
                cleanEditText();

                //scroll to top
                scrollView.scrollTo(0,0);
            }
        });
    }

    private void createOffer() {
        if (company.getText().toString().equals("") ||
                name.getText().toString().equals("") ||
                address.getText().toString().equals("") ||
                phone.getText().toString().equals("") ||
                fax.getText().toString().equals("") ||
                email.getText().toString().equals("") ||
                reference.getText().toString().equals("")) {
            Functions.toast(Constants.APP_CONTEXT, getString(R.string.pumpDetailsFragment_offlineOffers_completeFields), false);
        } else { //if all fields are filed
            Offer offer = new Offer();
            offer.company = company.getText().toString();
            offer.name = name.getText().toString();
            offer.address = address.getText().toString();
            offer.phone = phone.getText().toString();
            offer.fax = fax.getText().toString();
            offer.email = email.getText().toString();
            offer.reference = reference.getText().toString();
            //invisible offer vars
            offer.pumpID = mPump.id;
            offer.pumpName = mPump.getPumpName();
            offer.pumpChartUrl = mPump.chartUrl;
            //http://sealand.welldev.ro/home/generateImages?pump_id=697&x=20&y=50&foundx=20&foundy=51.2&eff=238&power=16.26&x_unit=l/s&y_unit=ft&name=Dorin&company=Welltech&email=dorin.iova@welltech.ro&address=Arad&phone=1234&fax=5678&reference=test&offer=2016092&lang=it_IT
            offer.pumpPdfPath = Constants.SITE_URL + "home/generateImages?pump_id=" + mPump.id;
            if (mPump.hasPerformanceValues) {
                offer.pumpPdfPath += "&x=" + mPump.x + "&y=" + mPump.y + "&foundx=" + mPump.foundX
                        + "&foundy=" + mPump.foundY + "&eff=" + (int) mPump.efficiency + "&power=" + mPump.power +
                        "&x_unit=" + mPump.xUnit + "&y_unit=" + mPump.yUnit;
            }
            offer.pumpPdfPath += "&name=" + offer.name + "&company=" + offer.company + "&email=" + offer.email +
                    "&address=" + offer.address + "&phone=" + offer.phone + "&fax=" + offer.fax +
                    "&reference=" + offer.reference + "&offer=" + offer.id + "&lang=" + (LocaleLanguageChanger.getIndexLanguage() == 0 ? "en_GB" : "it_IT");

            Gson gson = new Gson();
            //Functions.toast(Constants.APP_CONTEXT, gson.toJson(offer), true);
            String serializedOffers = Functions.readStringFromPrefs(Constants.APP_CONTEXT,
                    Constants.PREF_OFFERS_JSON, null);
            Offer[] offersList = gson.fromJson(serializedOffers, Offer[].class);
            Offer[] newOffersList;
            if (offersList != null) {
                newOffersList = new Offer[offersList.length + 1];
                for(int i = 0; i < offersList.length; i++) {
                    newOffersList[i] = offersList[i];
                }
                newOffersList[offersList.length] = offer; // last offer added is the new one just made
            } else {
                newOffersList = new Offer[1];
                newOffersList[0] = offer;
            }

            String newSerializedOffers = gson.toJson(newOffersList);
            Functions.writeStringToPrefs(Constants.APP_CONTEXT, Constants.PREF_OFFERS_JSON,
                    newSerializedOffers);

            Functions.toast(Constants.APP_CONTEXT, getString(R.string.pumpDetailsFragment_offlineOffers_newOffer), true);
        }
    }

    private void hideKeyboard() {
        try {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("HIDE KEYBOARD", e.getMessage());
        }
    }

    private void cleanEditText() {

        try{
            company.setText("");
            name.setText("");
            address.setText("");
            phone.setText("");
            fax.setText("");
            email.setText("");
            reference.setText("");
        } catch (Exception e) {
            Log.d("Clean Ex", "EditTextNull");
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_offline_offers, container, false);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();

                view.getWindowVisibleDisplayFrame(r);

                int h = view.getRootView().getHeight() - (r.bottom - r.top);
                if (h > 300) {
                    //Log.i("KEY", "IS open");
                    if(scrollingView != null) {
                        scrollingView.setVisibility(View.VISIBLE);
                    }
                } else {
                    //Log.i("KEY", "IS close");
                    scrollingView.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    public void setPumpForOffer(Pump pump) { // let offer know what pump we are talking about
        mPump = pump;
    }

    public void setPumpInfo(String name, String code) {
        mNamePump = name;
        mCodePump = code;

        if(mCodePumpTextView != null && mNamePumpTextView != null) {
            mNamePumpTextView.setText(name);
            mCodePumpTextView.setText(code);
        }
    }
}
