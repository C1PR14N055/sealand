package com.software.icebird.sealand.interfaces;

import android.view.View;

/**
 * Created by catalin on 01/09/2016.
 */
public interface OnClickListenerRecyclerView {
    void OnClickItem(View view, int position);
}
