package com.software.icebird.sealand.fragments.tabLayouts;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;

import java.io.File;

/**
 * Created by ch on 08.09.2016.
 */
public class CatalogFragment extends Fragment{

    private String mPdfPath;

    private TextView mNamePumpTextView;
    private String mNamePump = "";
    private TextView mCodePumpTextView;
    private String mCodePump = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View item = inflater.inflate(R.layout.fragment_catalog, container, false);

        mNamePumpTextView = (TextView) item.findViewById(R.id.textView_fragmentCatalog_pumpName);
        mNamePumpTextView.setText(mNamePump);
        mCodePumpTextView = (TextView) item.findViewById(R.id.textView_fragmentCatalog_pumpCode);
        mCodePumpTextView.setText(mCodePump);

        return item;
    }

    public void setPdfPath(String path) {
        mPdfPath = path;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ImageView imageView = (ImageView) getView().findViewById(R.id.fragment_catalog_imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPdf();
            }
        });
    }

    private void openPdf() {
        if (mPdfPath != null && !mPdfPath.equals("")) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                Uri uri = FileProvider.getUriForFile(Constants.APP_CONTEXT, "com.software.icebird.sealand",
                        new File(mPdfPath));
                //Functions.toast(Constants.APP_CONTEXT, String.valueOf(uri), true);
                //Log.d("IMAGE URI", String.valueOf(uri));
                intent.setDataAndType(uri, "application/pdf");
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Functions.toast(Constants.APP_CONTEXT, getString(R.string.offers_text_noPDFApplication), true);
                Log.d("PDF", mPdfPath);
                e.printStackTrace();
            }
            catch (Exception e) {
                Functions.toast(Constants.APP_CONTEXT, getString(R.string.offers_text_noPDFForPump), true);
                e.printStackTrace();
            }
        } else {
            Functions.toast(Constants.APP_CONTEXT, getString(R.string.offers_text_noPDFForPump), true);
        }
    }

    public void setPumpInfo(String name, String code) {

        mCodePump = code;
        mNamePump = name;

        if(mCodePumpTextView != null && mNamePumpTextView != null) {
            mNamePumpTextView.setText(name);
            mCodePumpTextView.setText(code);
        }
    }
}
