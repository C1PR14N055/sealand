package com.software.icebird.sealand.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.software.icebird.sealand.MenuManager;
import com.software.icebird.sealand.Pump;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.adapters.PumpAdapter;
import com.software.icebird.sealand.adapters.TextSpinnerAdapter;
import com.software.icebird.sealand.fragments.PumpDetailsFragment;
import com.software.icebird.sealand.fragments.PumpListFragment;
import com.software.icebird.sealand.interfaces.OnClickListenerRecyclerView;
import com.software.icebird.sealand.interfaces.OnRefreshActivity;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;
import com.software.icebird.sealand.utils.LocaleLanguageChanger;
import com.software.icebird.sealand.utils.ProgressDialogCreator;

import java.util.ArrayList;

public class SearchPumpByApplicationActivity extends AppCompatActivity implements OnClickListenerRecyclerView, OnRefreshActivity {

    private Context mContext;
    private PumpAdapter mPumpAdapter;
    private ArrayList<Pump> mSeriesList;

    private PumpListFragment mPumpListFragment;
    private PumpDetailsFragment mPumpDetailsFragment;

    private ProgressDialogCreator mLoadingProgressDialog;

    private int frequency = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_pump_by_application);

        init();
    }

    private void init() {

        mContext = SearchPumpByApplicationActivity.this;

        //init Context in Constants
        Constants.APP_CONTEXT = SearchPumpByApplicationActivity.this;

        LinearLayout linearLayout_appMenu = (LinearLayout) findViewById(R.id.linearLayout_searchPumpByApplication_appMenu);
        MenuManager menuManager = new MenuManager(mContext, linearLayout_appMenu, this);
        menuManager.initializeComponents();

        //init freq spinner
        final Spinner spinnerFreq = (Spinner) findViewById(R.id.spinner_searchPumpByApplication_frequency);
        TextSpinnerAdapter adapterFreq = new TextSpinnerAdapter(SearchPumpByApplicationActivity.this, R.array.pump_freq);
        spinnerFreq.setAdapter(adapterFreq);

        //init application spinner
        ArrayList<String> list = new ArrayList<>();
        try{
            Cursor cursor = Functions.execSQLGetCursor("SELECT * FROM Application WHERE BrandID = " +
                    Functions.getSelectedBrand(mContext) + " ORDER BY Sort DESC");
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);

                switch (LocaleLanguageChanger.getIndexLanguage()) {
                    case 0:{
                        list.add(cursor.getString(cursor.getColumnIndex("Title")));
                        break;
                    }

                    case 1: {
                        list.add(cursor.getString(cursor.getColumnIndex("Title__it_IT")));
                        break;
                    }

                    case 2: {
                        list.add(cursor.getString(cursor.getColumnIndex("Title__fr_FR")));
                        break;
                    }

                    case 3: {
                        list.add(cursor.getString(cursor.getColumnIndex("Title__es_ES")));
                        break;
                    }
                }
            }
            cursor.close();
        } catch (Exception e) {
            Log.e("SEARCH_APP", e.getMessage());
        }


        final Spinner spinnerApplication = (Spinner) findViewById(R.id.spinner_searchPumpByApplication_application);
        TextSpinnerAdapter adapterApplication = new TextSpinnerAdapter(SearchPumpByApplicationActivity.this, list);
        spinnerApplication.setAdapter(adapterApplication);

        Button searchButton = (Button) findViewById(R.id.button_searchPumpByApplication_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLoadingProgressDialog = new ProgressDialogCreator(mContext, getString(R.string.searchBy_commonFields_progressDialog_searchingPumps_title),
                        getString(R.string.searchBy_commonFields_progressDialog_searchingPumps_message), true, ProgressDialog.STYLE_SPINNER);
                mLoadingProgressDialog.showDialog();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String freq = spinnerFreq.getSelectedItem().toString();
                        String appl = spinnerApplication.getSelectedItem().toString();
                        searchPumpsByApplication(freq, appl);
                    }
                }).start();

            }
        });

    }

    private void searchPumpsByApplication(String freq, String appl){

        try{
            frequency = freq.equals("50Hz") ? 50 : 60;
            String translatedTitle = "Title";
            switch (LocaleLanguageChanger.getIndexLanguage()) {
                case 0:{
                    translatedTitle = "Title";
                    break;
                }
                case 1: {
                    translatedTitle = "Title__it_IT";
                    break;
                }
                case 2: {
                    translatedTitle = "Title__fr_FR";
                    break;
                }
                case 3: {
                    translatedTitle = "Title__es_ES";
                    break;
                }
            }
            ArrayList<Integer> applicationSeriesIDs;
            Cursor cursor;
            if (appl.equals(getString(R.string.item_spinner_allProduct))) {
                Log.d("selectedApplicationId", "all");
                applicationSeriesIDs = new ArrayList<>();

                cursor = Functions.execSQLGetCursor("SELECT * FROM Serie ORDER BY Title ASC;");
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    applicationSeriesIDs.add(cursor.getInt(cursor.getColumnIndex("ID")));
                }
            } else {
                String selectedApplicationId = Functions.execSQLGetFirstString("SELECT * FROM Application WHERE " +
                        translatedTitle + " = " + Functions.escapeSQL(appl) + " AND BrandID = " +
                        Functions.getSelectedBrand(mContext), "ID");
                Log.d("selectedApplicationId", selectedApplicationId);
                applicationSeriesIDs = new ArrayList<>();
                cursor= Functions.execSQLGetCursor("SELECT * FROM Application_Series WHERE ApplicationID = "
                        + selectedApplicationId);
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    applicationSeriesIDs.add(cursor.getInt(cursor.getColumnIndex("SerieID")));
                }
            }
            cursor.close();

            ArrayList<Integer> matchingFrequencySeriesIDs = new ArrayList<>();

            for (int i : applicationSeriesIDs) {

                Log.d("SQL", ("SELECT * FROM Pump WHERE SerieID = " + i +
                        " AND Frequency = " + frequency + " AND BrandID = " +
                        Functions.getSelectedBrand(mContext)));

                cursor = Functions.execSQLGetCursor("SELECT * FROM Pump WHERE SerieID = " + i +
                        " AND Frequency = " + frequency + " AND BrandID = " +
                        Functions.getSelectedBrand(mContext));
                if (cursor.getCount() > 0) {
                    matchingFrequencySeriesIDs.add(i);
                }
                cursor.close();
            }

            //if pump list is 0
            if(matchingFrequencySeriesIDs.size() == 0) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cleanPumpListFragment();
                        mLoadingProgressDialog.dismissDialog();
                        Functions.toast(mContext, getString(R.string.searchBy_noPumpFound), true);
                    }
                });

                return;
            }

            mSeriesList = new ArrayList<>();
            for (int i : matchingFrequencySeriesIDs) {
                //FIXME 50/60 & "For dewatering flooded areas & pits" => null, Serie ID = 23, Title = DV was not imported, """"" error
                String currentSerieName = Functions.execSQLGetFirstString("SELECT * FROM Serie WHERE ID = " + i, "Title");
                int firstPumpInSerieID = Functions.execSQLGetFirstInt("SELECT * FROM Pump WHERE SerieID = " + i, "ID");
                int firstPumpImageInPumpID = Functions.execSQLGetFirstInt("SELECT * FROM PumpImage WHERE OwnerID = " + firstPumpInSerieID, "ImageID");
                String thumbImageFilePath = Constants.THUMBS_DIR + "/" + Functions.execSQLGetFirstString("SELECT * FROM File WHERE ID = " + firstPumpImageInPumpID, "Title");
                mSeriesList.add(new Pump(currentSerieName, Constants.APP_CONTEXT.getFilesDir().getPath() + "/" + thumbImageFilePath));
            }

            Thread.sleep(400);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    createPumpListFragment(mSeriesList);
                }
            });

        } catch (Exception e) {
            Log.e("SEARCH_APP_SEARCH", e.getMessage());
        }
    }

    @Override
    public void OnClickItem(View view, int position) {
        //Toast.makeText(SearchPumpByApplicationActivity.this, mPumpAdapter.getPumpAt(position).getPumpName(), Toast.LENGTH_SHORT).show();

        String seriesName = mPumpAdapter.getPumpAt(position).getPumpName();

        createPumpDetailsFragment(seriesName);

    }

    private void createPumpListFragment(ArrayList<Pump> series) {

        Log.i("CREATE", "Create PumpListFragment");

        mPumpAdapter = new PumpAdapter(mContext, series, this, PumpAdapter.PUMP_IMAGE_WITH_TEXT);
        Constants.pumpAdapter = mPumpAdapter;

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if(mPumpDetailsFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpDetailsFragment).commit();
            mPumpDetailsFragment = null;
        }

        if(mPumpListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpListFragment).commit();
            Log.i("REMOVE", "Remove PumpListFragment");
        }

        mPumpListFragment = new PumpListFragment();

        transaction.add(R.id.linearLayout_searchPumpByApplication_fragmentContainer, mPumpListFragment);
        transaction.commit();

        mLoadingProgressDialog.dismissDialog();

    }

    private void createPumpDetailsFragment(String seriesName) {

        Bundle sendBundle = new Bundle();
        sendBundle.putInt(PumpDetailsFragment.TAG_CREATED_FROM, PumpDetailsFragment.TAG_CREATED_FROM_SERIE);
        sendBundle.putString("pumpSeries", seriesName);
        sendBundle.putInt("frequency", frequency);

        Log.i("CREATE", "Create PumpListFragment");

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if(mPumpListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpListFragment).commit();
            mPumpListFragment = null;
        }

        if(mPumpDetailsFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpDetailsFragment).commit();
            Log.i("REMOVE", "Remove PumpListFragment");
        }

        mPumpDetailsFragment = new PumpDetailsFragment();
        mPumpDetailsFragment.setArguments(sendBundle);

        transaction.add(R.id.linearLayout_searchPumpByApplication_fragmentContainer, mPumpDetailsFragment);
        transaction.commit();
    }

    /**
     * Clean the fragment when not found pumps
     */
    private void cleanPumpListFragment(){

        if(mPumpListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpListFragment).commit();
        }

        if(mPumpDetailsFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mPumpDetailsFragment).commit();
        }
    }

    @Override
    public void onRefreshActivity() {
        try{
            Intent refresh = new Intent(mContext, SearchPumpByApplicationActivity.class);
            startActivity(refresh);
            finish();
        } catch (Throwable e) {

        }
    }

}
