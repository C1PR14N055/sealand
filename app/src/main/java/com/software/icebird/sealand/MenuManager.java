package com.software.icebird.sealand;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.software.icebird.sealand.activities.DashboardActivity;
import com.software.icebird.sealand.activities.MainActivity;
import com.software.icebird.sealand.activities.OldOffersActivity;
import com.software.icebird.sealand.adapters.LanguageSpinnerAdapter;
import com.software.icebird.sealand.adapters.TextSpinnerAdapter;
import com.software.icebird.sealand.interfaces.OnRefreshActivity;
import com.software.icebird.sealand.services.DatabaseManagerService;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;
import com.software.icebird.sealand.utils.LocaleLanguageChanger;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by Catalin on 22.09.2016.
 */

public class MenuManager {

    private Context mContext;
    private View mLayoutView;

    private ImageView imageView_logo;
    private Button btn_oldOffers;
    private ImageButton btn_syncro;
    private Spinner spinner_user, spinner_language;
    private OnRefreshActivity mOnRefreshActivity;

    private LocaleLanguageChanger mLocaleLanguageChanger;

    public MenuManager(Context context, View layoutView, OnRefreshActivity onRefreshActivity) {

        this.mContext = context;
        this.mLayoutView = layoutView;
        this.mLocaleLanguageChanger = LocaleLanguageChanger.getInstance();

        this.mOnRefreshActivity = onRefreshActivity;
    }

    public void initializeComponents() {

        imageView_logo = (ImageView) mLayoutView.findViewById(R.id.imageView_menuApplication_logo);
        imageView_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!DashboardActivity.isActive) {
                    Intent dashboardIntent = new Intent(mContext, DashboardActivity.class);
                    mContext.startActivity(dashboardIntent);
                    ((Activity) mContext).finish();
                }
            }
        });
        btn_syncro = (ImageButton) mLayoutView.findViewById(R.id.buttonImage_menuApplication_synchronize);
        btn_syncro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!NetworkManager.getInstance(mContext).isNetworkConnected()) {
                    ((Activity)mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Functions.toast(mContext, mContext.getString(R.string.mainActivity_dialog_noInternet_message), true);
                        }
                    });

                    return;
                }

                if (Functions.getInternalSpaceAvailable() / 1000000 < 500.0) {
                    new android.support.v7.app.AlertDialog.Builder(mContext)
                            .setTitle(mContext.getString(R.string.mainActivity_dialog_runningLowOnSpace_title))
                            .setMessage(mContext.getString(R.string.mainActivity_dialog_runningLowOnSpace_message))
                            .setIcon(android.R.drawable.stat_sys_warning)
                            .setPositiveButton(mContext.getString(R.string.okButton), null)
                            .show();
                    return;
                }

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                alertDialog.setTitle(mContext.getString(R.string.menuApplication_synchronization_title));
                alertDialog.setMessage(mContext.getString(R.string.menuApplication_synchronization_message));
                alertDialog.setPositiveButton(mContext.getString(R.string.yesButton), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        updateData();
                    }
                });

                alertDialog.setNegativeButton(mContext.getString(R.string.noButton), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                alertDialog.show();
            }
        });

        if (NetworkManager.getInstance(mContext).isNetworkConnected()) {
            btn_syncro.setVisibility(View.VISIBLE);
        } else {
            btn_syncro.setVisibility(View.INVISIBLE);
        }

        btn_oldOffers = (Button) mLayoutView.findViewById(R.id.button_menuApplication_oldOffers);
        btn_oldOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*List<File> fileList = DatabaseManagerService.getAllFilesInPath(new File("/data/user/0/com.software.icebird.sealand/files/assets/Data"));
                for (File fl : fileList) {
                    Log.d("FILEPATH", fl.getAbsolutePath());
                }*/

                if (!OldOffersActivity.isActive) {
                    Intent intent = new Intent(Constants.APP_CONTEXT, OldOffersActivity.class);
                    Constants.APP_CONTEXT.startActivity(intent);
                    //Toast.makeText(mContext, "Old offers", Toast.LENGTH_SHORT).show();
                }
            }
        });


        spinner_user = (Spinner) mLayoutView.findViewById(R.id.spinner_menuApplication_user);

        final String[] userOptions = mContext.getResources().getStringArray(R.array.usersListSinner);

        final ArrayList<String> userListSpinner = new ArrayList<>();
        String userName = Functions.readStringFromPrefs(mContext, "user", "none");
        userListSpinner.add(userName);


        for (String opt : userOptions) {
            userListSpinner.add(opt);
        }

        TextSpinnerAdapter userSpinnerAdapter = new TextSpinnerAdapter(mContext, userListSpinner);
        spinner_user.setAdapter(userSpinnerAdapter);
        spinner_user.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String logout = parent.getItemAtPosition(position).toString();

                //log out option
                if (logout.equals(userOptions[0])) {
                    //Toast.makeText(mContext, userOptions[0], Toast.LENGTH_SHORT).show();
                    new AlertDialog.Builder(mContext)
                            .setTitle(mContext.getString(R.string.loginActivity_dialog_title_warning))
                            .setMessage(mContext.getString(R.string.menuApplication_logOutDataWillLost))
                            .setIcon(android.R.drawable.stat_sys_warning)
                            .setPositiveButton(mContext.getString(R.string.yesButton), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Functions.logOutUser(mContext);
                                    mContext.startActivity(new Intent(mContext, MainActivity.class));
                                    ((Activity) (mContext)).finish();
                                }
                            })
                            .setNegativeButton(mContext.getString(R.string.noButton), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    spinner_user.setSelection(0);
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner_language = (Spinner) mLayoutView.findViewById(R.id.spinner_menuApplication_language);
        final LanguageSpinnerAdapter languageSpinnerAdapter = new LanguageSpinnerAdapter(mContext);

        spinner_language.setAdapter(languageSpinnerAdapter);
        spinner_language.setSelection(mLocaleLanguageChanger.getIndexLanguage());

        spinner_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition = languageSpinnerAdapter.getLanguageIDFromIndex(position);

                if (itemPosition != mLocaleLanguageChanger.getIndexLanguage()) {
                    switch (itemPosition) {
                        case 0: {
                            mLocaleLanguageChanger.setDeviceLanguage(mContext, "en");
                            mOnRefreshActivity.onRefreshActivity();
                            break;
                        }
                        case 1: {
//                            Toast.makeText(mContext, "Italian language not available yet", Toast.LENGTH_SHORT).show();
                            mLocaleLanguageChanger.setDeviceLanguage(mContext, "it");
                            mOnRefreshActivity.onRefreshActivity();
                            break;
                        }
                        case 2: {
                            Toast.makeText(mContext, "French language not available yet", Toast.LENGTH_SHORT).show();
                            spinner_language.setSelection(LocaleLanguageChanger.getIndexLanguage());
                            //mLocaleLanguageHelper.setDeviceLanguage(mContext, "fr");
//                            mOnRefreshActivity.onRefreshActivity();
                            break;
                        }
                        case 3: {
                            Toast.makeText(mContext, "Spanish Language not available yet", Toast.LENGTH_SHORT).show();
                            spinner_language.setSelection(LocaleLanguageChanger.getIndexLanguage());
                            //mLocaleLanguageHelper.setDeviceLanguage(mContext, "es");
//                            mOnRefreshActivity.onRefreshActivity();
                            break;
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void updateData() {
        final StringBuilder url = new StringBuilder();
        url.append(Constants.SITE_URL);
        url.append("home/export?token=");
        url.append(Functions.readStringFromPrefs(mContext, Constants.PREF_SECRET_TOKEN_KEY, ""));
        final String lastFileId = Functions.execSQLGetFirstString("SELECT * FROM File ORDER BY ID DESC LIMIT 1", "ID");
        Log.d("LAST_FILE_ID", lastFileId);
        //url.append("&file_id=1747"); // Test
        url.append(lastFileId);

        Log.i("URL", url.toString());

        new Thread(new Runnable() {
            @Override
            public void run() {

                URL urll;
                try {
                    urll = new URL(url.toString());

                    URLConnection connection = urll.openConnection();
                    connection.setConnectTimeout(5000);
                    connection.connect();

                    InputStream receivedStream = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(receivedStream));
                    final StringBuilder responseJsonString = new StringBuilder();

                    String line;
                    while ((line = reader.readLine()) != null) {
                        responseJsonString.append(line);
                    }

                    //close streams
                    receivedStream.close();
                    reader.close();

                    ((Activity)mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try{
                                Log.d("UPDATE RESP", responseJsonString.toString());
                                final JSONObject json = new JSONObject(responseJsonString.toString());
                                String jsonResp = json.getString("response");
                                if (jsonResp.equals("Update needed")) {

                                    Intent intent = new Intent(mContext, DatabaseManagerService.class);
                                    intent.putExtra(DatabaseManagerService.TAG_RECEIVER,
                                            new MainActivity.DownloadReceiver(new Handler(Looper.getMainLooper())));
                                    intent.putExtra(DatabaseManagerService.TAG_DATABASE_MANAGER_TYPE,
                                            DatabaseManagerService.UPDATE_DATABASE_TYPE);
                                    intent.putExtra(DatabaseManagerService.TAG_FILE_ID, lastFileId);

                                    //start dialog
                                    MainActivity.showDialog();

                                    mContext.startService(intent);
                                    //Functions.toast(Constants.APP_CONTEXT, mContext.getString(R.string.sync_updateStarted_message),
                                    //        true);

                                } else {
                                    Functions.toast(Constants.APP_CONTEXT,
                                            mContext.getString(R.string.menuApplication_fileUpdated), true);
                                }
                            } catch (Exception e) {
                                Log.e("ERROR", e.getMessage());
                            }

                        }
                    });


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        //Toast.makeText(mContext, mContext.getString(R.string.menuApplication_fileUpdated), Toast.LENGTH_SHORT).show();
    }
}
