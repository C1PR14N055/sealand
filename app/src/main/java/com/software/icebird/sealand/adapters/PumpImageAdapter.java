package com.software.icebird.sealand.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.adapters.viewHolders.PumpViewWithoutTextViewHolder;
import com.software.icebird.sealand.interfaces.OnClickListenerRecyclerView;

import java.io.File;
import java.util.List;

/**
 * Created by Catalin on 28.09.2016.
 */

public class PumpImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<String> mImagePathList;

    private OnClickListenerRecyclerView mListenerRecyclerView;

    public PumpImageAdapter(Context context, List<String> imagePathList, OnClickListenerRecyclerView listenerRecyclerView) {

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mImagePathList = imagePathList;
        mListenerRecyclerView = listenerRecyclerView;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = mInflater.inflate(R.layout.pump_layout_without_text, parent, false);

        return new PumpViewWithoutTextViewHolder(item, mListenerRecyclerView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof PumpViewWithoutTextViewHolder) {
            final PumpViewWithoutTextViewHolder pumpViewHolder = (PumpViewWithoutTextViewHolder) holder;

            final String path = mImagePathList.get(position);

            File imageFile = new File(path);

            if(imageFile.exists()) {
                new Thread(new Runnable() {
                    public void run() {
                    final Bitmap bitmap =
                            BitmapFactory.decodeFile(path);
                    pumpViewHolder.mPumpImageView.post(new Runnable() {
                        public void run() {
                            pumpViewHolder.mPumpImageView.setImageBitmap(bitmap);
                            //stop loading image
                            pumpViewHolder.mPumpLoadingImage.setVisibility(View.GONE);
                            pumpViewHolder.mPumpImageView.refreshDrawableState();
                        }
                    });
                }
            }).start();
        } else {
            pumpViewHolder.mPumpImageView.setImageResource(R.drawable.none_pump);
            pumpViewHolder.mPumpLoadingImage.setVisibility(View.GONE);
            pumpViewHolder.mPumpImageView.refreshDrawableState();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mImagePathList.size();
    }
}
