package com.software.icebird.sealand.adapters.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.interfaces.OnClickListenerRecyclerView;

/**
 * Created by Catalin on 15.09.2016.
 */
public class PumpViewWithoutTextViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public ImageView mPumpImageView;
    public ProgressBar mPumpLoadingImage;

    private OnClickListenerRecyclerView mListenerRecyclerView;

    public PumpViewWithoutTextViewHolder(View itemView, OnClickListenerRecyclerView listenerRecyclerView) {
        super(itemView);

        mListenerRecyclerView = listenerRecyclerView;
        itemView.setOnClickListener(this);

        mPumpImageView = (ImageView) itemView.findViewById(R.id.imageView_pumpLayout_imagePump);
        mPumpLoadingImage = (ProgressBar) itemView.findViewById(R.id.progressBar_pumpLayout_loadingImage);
    }

    @Override
    public void onClick(View v) {
        mListenerRecyclerView.OnClickItem(itemView, this.getLayoutPosition());
    }
}
