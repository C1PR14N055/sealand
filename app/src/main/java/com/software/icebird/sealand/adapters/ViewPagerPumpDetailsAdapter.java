package com.software.icebird.sealand.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ch on 08.09.2016.
 */
public class ViewPagerPumpDetailsAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mTitleFragmentList = new ArrayList<>();

    public ViewPagerPumpDetailsAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void addItem(Fragment fragment, String titleFragment) {
        mFragmentList.add(fragment);
        mTitleFragmentList.add(titleFragment);
    }

    public void removeItem(Fragment fragment, String titleFragment) {
        mFragmentList.remove(fragment);
        mTitleFragmentList.remove(titleFragment);
    }

    public void removeItemAtIndex(int index) {
        if(mFragmentList.size() <= index) {
            mFragmentList.remove(index);
            mTitleFragmentList.remove(index);
        }
    }

    public List<Fragment> getFragmentList() {
        return mFragmentList;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleFragmentList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
