package com.software.icebird.sealand.utils;

import android.content.Context;

import com.software.icebird.sealand.adapters.PumpAdapter;

/**
 * Created by catalin on 8/16/16.
 */
public class Constants {
    //keeps application context
    public static Context APP_CONTEXT = null;

    public static PumpAdapter pumpAdapter;

    public static final String TEXT_FONT_NAME = "fonts/Ubuntu-R.ttf";

    public static final String SITE_URL = "http://sealand.welldev.ro/";

    public static final String DEBUG_TAG = "DEBUG";

    //preferences file name
    public static final String PREF_FILE_NAME = "prefs";
    //preferences keys, used to check the shared preferences
    public static final String PREF_SECRET_TOKEN_KEY = "PREF_SECRET_TOKEN_KEY";
    public static final String PREF_INITIAL_DB_INSTALLED_KEY = "PREF_INITIAL_DB_INSTALLED_KEY";

    //logged in member email address
    public static final String PREF_LOGGED_IN_MEMBER_EMAIL = "PREF_LOGGED_IN_MEMBER_EMAIL";

    public static final String THUMBS_DIR = "assets/Data/Picture/Thumb";
    public static final String THUMBS_DIR_NAME = "Thumb";

    //brands pref
    public static final String PREF_BRAND = "PREF_BRAND";
    public static final int PREF_NO_BRAND_SELECTED = 0;
    public static final int PREF_SEALAND_BRAND = 1;
    public static final int PREF_AQUA_BRAND = 2;
    public static final int PREF_SIXTEAM_BRAND = 3;

    //offers pref string for serializing json
    public static final String PREF_OFFERS_JSON = "PREF_OFFERS_JSON";

    public static final String GOOGLE_PDF_VIEWER_API = "https://docs.google.com/gview?embedded=true&url=";

}
