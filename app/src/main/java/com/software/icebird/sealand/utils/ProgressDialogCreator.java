package com.software.icebird.sealand.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by catalin on 8/22/16.
 */
public class ProgressDialogCreator {

    private ProgressDialog mProgressDialog;

    public ProgressDialogCreator(Context context, String title, String message, boolean cancelable, int styleDialog) {

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(cancelable);
        mProgressDialog.setProgressStyle(styleDialog);
    }

    public ProgressDialogCreator(Context context, String title, String message) {

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    public void showDialog() {
        if(mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    public boolean isShowing() {
        if (mProgressDialog != null) {
            return mProgressDialog.isShowing();
        }
        return false;
    }

    public void dismissDialog() {
        if(mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void setProgress(int progress) {
        if(mProgressDialog != null) {
            mProgressDialog.setProgress(progress);
        }
    }

    public void setMaxProgress(int maxProgress) {
        if(mProgressDialog != null) {
            mProgressDialog.setMax(maxProgress);
        }
    }

    public void setIndeterminate(boolean status) {
        if(mProgressDialog != null) {
            mProgressDialog.setIndeterminate(status);
        }
    }
    public void incrementProcess(int process) {
        mProgressDialog.incrementProgressBy(process);
    }

    public void setSecondaryProgress(int secondaryProgress) {
        if(mProgressDialog != null) {
            mProgressDialog.setSecondaryProgress(secondaryProgress);

        }
    }

    public void setTitle(String title) {
        if(mProgressDialog != null) {
            mProgressDialog.setTitle(title);
        }
    }

    public void setMessage(String message) {
        if(mProgressDialog != null) {
            mProgressDialog.setMessage(message);
        }
    }


}
