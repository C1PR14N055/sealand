package com.software.icebird.sealand.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.software.icebird.sealand.NetworkManager;
import com.software.icebird.sealand.Pump;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.adapters.ElementsListViewAdapter;
import com.software.icebird.sealand.adapters.ViewPagerPumpDetailsAdapter;
import com.software.icebird.sealand.fragments.tabLayouts.CatalogFragment;
import com.software.icebird.sealand.fragments.tabLayouts.ImagesFragment;
import com.software.icebird.sealand.fragments.tabLayouts.OfflineOffersFragment;
import com.software.icebird.sealand.fragments.tabLayouts.PrintPreviewFragment;
import com.software.icebird.sealand.fragments.tabLayouts.PumpChartFragment;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;
import com.software.icebird.sealand.utils.LocaleLanguageChanger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Catalin on 24.09.2016.
 */

public class PumpDetailsFragment extends Fragment {

    //var
    private Context mContext;
    private List<Pump> mPumpList;
    private Pump mPump;
    private int mPumpPosition;
    private ViewPagerPumpDetailsAdapter viewPagerPumpDetailsAdapter;
    private String mPumpName;
    private String mPumpCode;
    private ElementsListViewAdapter elementsListViewAdapter;

    //constants
    public static final String TAG_CREATED_FROM = "TAG_CREATED_FROM";
    public static final int TAG_CREATED_FROM_SERIE = 1; // fragment created from series, gets ID
    public static final int TAG_CREATED_FROM_PUMP = 2; // fragment created from PUMP, get Pump obj

    //components
    private TextView mProductNameTextView;
    private ListView mElementsListView;
    private ViewPager mPumpDetailsViewPager;
    private int lastViewPager = 0;

    //fragments
    private PumpChartFragment mPumpChartFragment;
    private PrintPreviewFragment mPrintPreviewFragment;
    private OfflineOffersFragment mOfflineOffersFragment;
    private CatalogFragment mCatalogFragment;
    private ImagesFragment mImagesFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.fragment_pump_details, container, false);

        mProductNameTextView = (TextView) itemView.findViewById(R.id.textView_fragmentPumpDetails_seriesName);

        parseArguments();

        mElementsListView = (ListView) itemView.findViewById(R.id.listView_fragmentPumpDetails_seriesElements);
        mElementsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mPump = mPumpList.get(position);
                Log.e("id", "" + mPump.id);

                setPumpInfo(mPump.id);

                changeDataInFragment();

                elementsListViewAdapter.setSelectedPosition(position);
                elementsListViewAdapter.notifyDataSetChanged();
            }
        });

        setListViewAdapter();

        mPumpDetailsViewPager = (ViewPager) itemView.findViewById(R.id.viewPager_fragmentPumpDetails);
        mPumpDetailsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (NetworkManager.getInstance(getContext()).isNetworkConnected()) {
                    switch (position){
                        case 0: {
                            setChartFragmentData();
                            break;
                        }

                        case 1: {
                            setPrintPreviewFragmentData();
                            break;
                        }

                        case 2:{
                            setOldOffersFragmentData();
                            break;
                        }

                        case 3:{
                            setCatalogFragmentData();
                            break;
                        }

                        case 4: {
                            setImageFragmentData();
                            break;
                        }
                    }
                } else {
                    switch (position){
                        case 0: {
                            setOldOffersFragmentData();
                            break;
                        }

                        case 1: {
                            setCatalogFragmentData();
                            break;
                        }

                        case 2:{
                            setImageFragmentData();
                            break;
                        }
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setViewPager();

        TabLayout mTabLayoutPumpDetails = (TabLayout) itemView.findViewById(R.id.tabLayout_fragmentPumpDetails);
        mTabLayoutPumpDetails.setupWithViewPager(mPumpDetailsViewPager);

        return itemView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        deleteFragments();

        mPumpList.clear();
    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

    private void parseArguments() {
        Bundle receivedBundle = getArguments();

        int option = receivedBundle.getInt(TAG_CREATED_FROM);

        switch (option) {
            //PUMP SERIES
            case 1:{
                getDataFromApplication(receivedBundle);
                break;
            }

            //PUMP OBJECT
            case 2: {
                getDataFromPerformance(receivedBundle);
                break;
            }
        }
    }

    /**
     * Parse bundle recevied from SearchByPerformance
     * @param receivedBundle
     */
    private void getDataFromPerformance(Bundle receivedBundle) {
        mPumpPosition = receivedBundle.getInt("pumpPosition");

        mPumpList = Constants.pumpAdapter.getPumpList();

        //create URL CHART for each pump
        for(Pump p : mPumpList) {
            p.hasPerformanceValues = true;

            StringBuilder builderChartUrl = new StringBuilder();
            builderChartUrl.append(Constants.SITE_URL);
            builderChartUrl.append("home/");
            builderChartUrl.append("previewChart?pump_id=");
            builderChartUrl.append(p.id);
            builderChartUrl.append("&x=");
            builderChartUrl.append(p.x);
            builderChartUrl.append("&y=");
            builderChartUrl.append(p.y);
            builderChartUrl.append("&foundx=");
            builderChartUrl.append(p.foundX);
            builderChartUrl.append("&foundy=");
            builderChartUrl.append(p.foundY);
            builderChartUrl.append("&eff=");
            builderChartUrl.append(p.efficiency);
            builderChartUrl.append("&power=");
            if (p.power > -1) {
                builderChartUrl.append(p.power);
            }
            builderChartUrl.append("&npsh=");
            if (p.npsh > -1) {
                builderChartUrl.append(p.npsh);
            }
            builderChartUrl.append("&x_unit=");
            builderChartUrl.append(p.xUnit);
            builderChartUrl.append("&y_unit=");
            builderChartUrl.append(p.yUnit);

            p.chartUrl = builderChartUrl.toString();
        }

        mPump = mPumpList.get(mPumpPosition);

        //get name and code
        setPumpInfo(mPump.id);

        mProductNameTextView.setText(getString(R.string.pumpDetailsFragment_seriesName, ""));
    }

    /**
     * Parse bundle recevied from SearchByApplication
     * @param receivedBundle
     */
    private void getDataFromApplication(Bundle receivedBundle) {
        String seriesName = receivedBundle.getString("pumpSeries");
        int frequency = receivedBundle.getInt("frequency");
        mProductNameTextView.setText(getString(R.string.pumpDetailsFragment_seriesName, seriesName));

        int brandID = Functions.getSelectedBrand(mContext);

        int serieID = Functions.execSQLGetFirstInt("SELECT * FROM Serie WHERE Title = " +
                Functions.escapeSQL(seriesName), "ID");

        mPumpList = new ArrayList<>();

        try{
            Cursor cursor = Functions.execSQLGetCursor("SELECT * FROM Pump WHERE SerieID = " + serieID + " AND BrandID = " + brandID + " AND Frequency = " + frequency);

            for(int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);

                int pumpIDIndex = cursor.getColumnIndex("ID");
                int pumpNameIndex = cursor.getColumnIndex("Title");

                int pumpID = cursor.getInt(pumpIDIndex);
                String pumpName = cursor.getString(pumpNameIndex);

                Pump p = new Pump();

                p.id = pumpID;
                p.setPumpName(pumpName);
                p.chartUrl = Constants.SITE_URL + "home/previewChart?pump_id=" + p.id;

                int catalogID = Functions.execSQLGetFirstInt("SELECT * FROM Pump WHERE ID = " + p.id, "CatalogID");
                String pdfPath = Functions.execSQLGetFirstString("SELECT * FROM File WHERE ID = " + catalogID, "Filename");
                p.pumpPdfPath = Constants.APP_CONTEXT.getFilesDir().getPath() + "/" + pdfPath;

                mPumpList.add(p);
            }

            cursor.close();

        }catch (Exception e) {
            Log.e("DATABASE", e.getMessage());
        }

        //first pump
        mPump = mPumpList.get(0);

        //get pump name and code
        setPumpInfo(mPump.id);
    }

    /**
     * Set left list view elements
     */
    private void setListViewAdapter() {
        ArrayList<String> nameList = new ArrayList<>();

        for(Pump p : mPumpList) {
            nameList.add(p.getPumpName());
        }

//        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.item_series_element,
//                R.id.textView_seriesElement_name, nameList);
//        mElementsListView.setAdapter(adapter);

        elementsListViewAdapter = new ElementsListViewAdapter(mContext, nameList);
        elementsListViewAdapter.setSelectedPosition(0);

        mElementsListView.setAdapter(elementsListViewAdapter);
    }

    /**
     * Set adapter for tab fragments
     */
    private void setViewPager() {
        viewPagerPumpDetailsAdapter = new ViewPagerPumpDetailsAdapter(getFragmentManager());

        createFragments();

        if (NetworkManager.getInstance(getContext()).isNetworkConnected()) {
            viewPagerPumpDetailsAdapter.addItem(mPumpChartFragment,getString(R.string.pumpDetailsFragment_chart_tabName));
            viewPagerPumpDetailsAdapter.addItem(mPrintPreviewFragment, getString(R.string.pumpDetailsFragment_print_preview_tabName));
        }
        viewPagerPumpDetailsAdapter.addItem(mOfflineOffersFragment, getString(R.string.pumpDetailsFragment_oldOffers_tabName));
        viewPagerPumpDetailsAdapter.addItem(mCatalogFragment, getString(R.string.pumpDetailsFragment_catalog_tabName));
        viewPagerPumpDetailsAdapter.addItem(mImagesFragment, getString(R.string.pumpDetailsFragment_images_tabName));

        mPumpDetailsViewPager.setAdapter(viewPagerPumpDetailsAdapter);
        viewPagerPumpDetailsAdapter.notifyDataSetChanged();

        changeDataInFragment();

    }

    private void createFragments() {

        try{
            if (NetworkManager.getInstance(getContext()).isNetworkConnected()) {
                mPumpChartFragment = new PumpChartFragment();
                mPrintPreviewFragment = new PrintPreviewFragment();
            }

            mOfflineOffersFragment = new OfflineOffersFragment();
            mCatalogFragment = new CatalogFragment();
            mImagesFragment = new ImagesFragment();
        } catch (Exception e) {
            Log.e("PMP_DETAILS_CREATE_FRAG", e.getMessage());
        }

    }

    private void deleteFragments() {
        try{
            if(mPumpChartFragment != null)
                getFragmentManager().beginTransaction().remove(mPumpChartFragment).commit();
            if(mPrintPreviewFragment != null)
                getFragmentManager().beginTransaction().remove(mPrintPreviewFragment).commit();
            if(mOfflineOffersFragment != null)
                getFragmentManager().beginTransaction().remove(mOfflineOffersFragment).commit();
            if(mCatalogFragment != null)
                getFragmentManager().beginTransaction().remove(mCatalogFragment).commit();
            if(mImagesFragment != null)
                getFragmentManager().beginTransaction().remove(mImagesFragment).commit();
            mPumpDetailsViewPager.removeAllViews();
        }catch (Exception e ) {
            Log.e("FRAGMENT REMOVE", e.getMessage());
        }
    }

    /**
     * Fill fragments with data when they are created
     */
    private void changeDataInFragment() {
        setChartFragmentData();

        setPrintPreviewFragmentData();

        setCatalogFragmentData();

        setOldOffersFragmentData();

        setImageFragmentData();
    }

    private void setChartFragmentData() {

        try{
            if (NetworkManager.getInstance(getContext()).isNetworkConnected()) {
                mPumpChartFragment.setUrlToLoad(mPump.chartUrl);
                mPumpChartFragment.setPumpInfo(mPumpName, mPumpCode);
                mPumpChartFragment.loadUrl();
                mPumpChartFragment.createTextField();
            }
        } catch (Exception e) {

            String error = "";

            if(e.getMessage() != null) {
                error = e.getMessage();
            }
            Log.i("PMP_DETAILS_SETDATA", error);
        }

    }

    private void setPrintPreviewFragmentData() {
        try{
            if (NetworkManager.getInstance(getContext()).isNetworkConnected()) {

                String url = Constants.SITE_URL + "home/generateImages?pump_id=" + mPump.id;
                if (mPump.hasPerformanceValues) {
                    url += "&x=" + mPump.x + "&y=" + mPump.y + "&foundx=" + mPump.foundX
                            + "&foundy=" + mPump.foundY + "&eff=" + (int) mPump.efficiency + "&power=" + mPump.power +
                            "&x_unit=" + mPump.xUnit + "&y_unit=" + mPump.yUnit;
                }
                url += "&lang=" + (LocaleLanguageChanger.getIndexLanguage() == 0 ? "en_GB" : "it_IT");

                mPrintPreviewFragment.setUrlToLoad(url, mPump.id);
                mPrintPreviewFragment.setPumpInfo(mPumpName, mPumpCode);
                mPrintPreviewFragment.loadUrl();
                mPrintPreviewFragment.createTextField();
            }
        } catch (Exception e) {
            Log.e("PMP_DETAILS_PRINTPREV", e.getMessage());
        }


    }

    private void setOldOffersFragmentData() {
        mOfflineOffersFragment.setPumpForOffer(mPump);
        mOfflineOffersFragment.setPumpInfo(mPumpName, mPumpCode);
    }

    private void setCatalogFragmentData() {
        mCatalogFragment.setPdfPath(mPump.pumpPdfPath);
        mCatalogFragment.setPumpInfo(mPumpName, mPumpCode);
    }

    private void setImageFragmentData() {
        mImagesFragment.getImageFromDatabase(mPump.id, mContext);
        mImagesFragment.setPumpInfo(mPumpName, mPumpCode);
    }

    /**
     * Get from database the pump name and pump code
     * @param id
     */
    private void setPumpInfo(int id) {
        mPumpName = mPump.getPumpName();
        mPumpCode = getString(R.string.pumpDetailsFragment_pumpCode) + Functions.execSQLGetFirstString("SELECT * FROM Pump WHERE ID = " + id, "Code");
    }
}