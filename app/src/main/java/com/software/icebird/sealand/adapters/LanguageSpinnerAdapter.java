package com.software.icebird.sealand.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.software.icebird.sealand.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ch on 23.09.2016.
 */

public class LanguageSpinnerAdapter extends ArrayAdapter<Integer> {

    private Context mContext;
    private List<Language> languageList;

    private static Integer[] sImageArray = {
        R.drawable.gb,
        R.drawable.it,
        R.drawable.fr,
        R.drawable.es
    };

    public LanguageSpinnerAdapter(Context context) {
        super(context, R.layout.item_spinner_language, R.id.imageView_spinnerLanguageItem_language, sImageArray);

        this.mContext = context;

        setLanguageList();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    public View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);

        View item = inflater.inflate(R.layout.item_spinner_language, parent, false);

        ImageView languageImageView = (ImageView) item.findViewById(R.id.imageView_spinnerLanguageItem_language);
        languageImageView.setImageResource(languageList.get(position).getImageID());

        return item;
    }

    public int getLanguageIDFromIndex(int index) {
        return languageList.get(index).getLanguageID();
    }

    public void setLanguageList() {
        languageList = new ArrayList<>();

        Language en = new Language(0, R.drawable.gb);
        languageList.add(en);
        Language it = new Language(1, R.drawable.it);
        languageList.add(it);
        Language fr = new Language(2, R.drawable.fr);
        languageList.add(fr);
        Language es = new Language(3, R.drawable.es);
        languageList.add(es);
    }

    private class Language {
        private int mLanguageID;
        private int mImageID;

        public Language(int id, int image){
            this.mLanguageID = id;
            this.mImageID = image;
        }

        public int getLanguageID() {
            return mLanguageID;
        }

        public void setLanguageID(int mLanguageID) {
            this.mLanguageID = mLanguageID;
        }

        public int getImageID() {
            return mImageID;
        }

        public void setImageID(int mImageID) {
            this.mImageID = mImageID;
        }
    }
}
