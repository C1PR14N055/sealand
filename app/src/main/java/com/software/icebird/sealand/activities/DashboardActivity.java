package com.software.icebird.sealand.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.software.icebird.sealand.MenuManager;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.interfaces.OnRefreshActivity;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;

public class DashboardActivity extends AppCompatActivity implements OnRefreshActivity {

    private Context mContext;

    public static boolean isActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isActive = true;
    }

    private void init(){
        //init Context in Constants
        Constants.APP_CONTEXT = DashboardActivity.this;

        mContext = DashboardActivity.this;

        LinearLayout linearLayout_appMenu = (LinearLayout) findViewById(R.id.linearLayout_dashboard_appMenu);

        //MenuManager menuManager = new MenuManager(mContext, linearLayout_appMenu);
        MenuManager menuManager = new MenuManager(mContext, linearLayout_appMenu, this);
        menuManager.initializeComponents();

        TextView textView_welcomeMessage = (TextView) findViewById(R.id.textView_dashboard_welcomeMessage);
        String user = Functions.readStringFromPrefs(DashboardActivity.this, "user", "");
        textView_welcomeMessage.setText(getString(R.string.dashboardActivity_textView_user_welcomeMessage, user));

        //Deselect previous brand
        writeBrandToSharePreference(Constants.PREF_NO_BRAND_SELECTED);

        //Select brand and write it to share pref
        final ImageView imageView_logoSealand = (ImageView) findViewById(R.id.imageView_dashboard_logoSealand);
        final ImageView imageView_logoSixTeam = (ImageView) findViewById(R.id.imageView_dashboard_logoSixTeam);

        imageView_logoSealand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeBrandToSharePreference(Constants.PREF_SEALAND_BRAND);
                imageView_logoSealand.setImageResource(R.drawable.selected_sealand_brand);
                imageView_logoSixTeam.setImageResource(R.drawable.unselected_six_team_brand);
            }
        });

        imageView_logoSixTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeBrandToSharePreference(Constants.PREF_SIXTEAM_BRAND);
                imageView_logoSixTeam.setImageResource(R.drawable.selected_six_team_brand);
                imageView_logoSealand.setImageResource(R.drawable.unselected_sealand_brand);
            }
        });

//        ImageView imageView_logoAqua = (ImageView) findViewById(R.id.imageView_dashboard_logoAqua);
//        imageView_logoAqua.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                writeBrandToSharePreference(Constants.TAG_AQUA_BRAND);
//            }
//        });

        RelativeLayout button_search_by_perf = (RelativeLayout) findViewById(R.id.layout_search_by_perf);
        RelativeLayout button_search_by_app = (RelativeLayout) findViewById(R.id.layout_search_by_app);

        button_search_by_perf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Functions.getSelectedBrand(mContext) > 0) {
                    Intent intent = new Intent(mContext, SearchPumpByPerformanceActivity.class);
                    startActivity(intent);
                } else {
                    notSelectedBrandMessage();
                }
            }
        });

        button_search_by_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Functions.getSelectedBrand(mContext) > 0) {
                    Intent intent = new Intent(mContext, SearchPumpByApplicationActivity.class);
                    startActivity(intent);
                } else {
                    notSelectedBrandMessage();
                }
            }
        });
    }

    private void notSelectedBrandMessage() {
        final AlertDialog.Builder brandDialog = new AlertDialog.Builder(mContext);

        brandDialog.setTitle(R.string.dashboardActivity_dialog_notSelectedBrand_title);
        brandDialog.setMessage(R.string.dashboardActivity_dialog_notSelectedBrand_message);
        brandDialog.setPositiveButton(R.string.okButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        brandDialog.show();
    }

    private void writeBrandToSharePreference(int brandID) {
        Functions.writeIntToPrefs(mContext, Constants.PREF_BRAND, brandID);
    }

    @Override
    public void onRefreshActivity() {
        try{
            Intent refresh = new Intent(mContext, DashboardActivity.class);
            startActivity(refresh);
            finish();
        } catch (Throwable e) {

        }
    }
}
