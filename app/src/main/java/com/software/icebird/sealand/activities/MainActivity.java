package com.software.icebird.sealand.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.software.icebird.sealand.NetworkManager;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.services.DatabaseManagerService;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.FontChanger;
import com.software.icebird.sealand.utils.Functions;
import com.software.icebird.sealand.utils.LocaleLanguageChanger;
import com.software.icebird.sealand.utils.ProgressDialogCreator;
import com.software.icebird.sealand.utils.RequestAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements RequestAsyncTask.OnTaskCompleted {

    private static String current_token = "";
    private LocaleLanguageChanger mLocaleLanguageChanger;

    static ProgressDialogCreator mProgressDialog;
    static boolean progressDialogShowing = false;

    private static final int PERMISSION_REQ = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setLanguage(); //set language before loading layout
        FontChanger.overrideFont(MainActivity.this, "SERIF", "fonts/Ubuntu-R.ttf");

        setContentView(R.layout.activity_main);

        //check if device is compatible
        isDeviceTablet(MainActivity.this);

        init();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressDialog != null) {
            mProgressDialog.dismissDialog();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mProgressDialog != null && !progressDialogShowing) {
            mProgressDialog.dismissDialog();
        } else if (mProgressDialog != null && progressDialogShowing) {
            setProgressDialogContent(DatabaseManagerService.sCurrentStep);
            mProgressDialog.showDialog();
        } else {
            if (mProgressDialog == null && progressDialogShowing) {
                showDialog();
                setProgressDialogContent(DatabaseManagerService.sCurrentStep);
            }
        }

        try{
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(DatabaseManagerService.NOTIF_ID);
        } catch (Exception e) {
            Log.e("MAIN_ONRESUME_NOTIFY", e.getMessage());
        }


    }

    private void init() {
        //save application context in constants to have access at it in other class
        Constants.APP_CONTEXT = MainActivity.this;

        Button accessButton = (Button) findViewById(R.id.button_main_open);
        accessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.readStringFromPrefs(getApplicationContext(),
                        Constants.PREF_INITIAL_DB_INSTALLED_KEY, "").equals(
                        Constants.PREF_INITIAL_DB_INSTALLED_KEY)) {

                    if (!Functions.readStringFromPrefs(MainActivity.this, Constants.PREF_LOGGED_IN_MEMBER_EMAIL,
                            "").equals("")) {
                        Log.d("USER LOGGED IN", Functions.readStringFromPrefs(MainActivity.this,
                                Constants.PREF_LOGGED_IN_MEMBER_EMAIL, ""));
                        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                        startActivity(intent);
                    } else {
                        Log.d("USER NEEDS TO LOG IN", "TRUE");
                        boolean needsRead = ActivityCompat.checkSelfPermission(MainActivity.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED;
                        boolean needsWrite = ActivityCompat.checkSelfPermission(MainActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED;
                        if (needsRead || needsWrite) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSION_REQ);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }
                } else {
                    Log.d("space", String.valueOf(Functions.getInternalSpaceAvailable()));
                    if (Functions.getInternalSpaceAvailable() / 1000000 < 500.0) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle(getString(R.string.mainActivity_dialog_runningLowOnSpace_title))
                                .setMessage(getString(R.string.mainActivity_dialog_runningLowOnSpace_message))
                                .setIcon(android.R.drawable.stat_sys_warning)
                                .setPositiveButton(getString(R.string.okButton), null)
                                .show();
                    } else {
                        try{
                            NetworkManager networkManager = NetworkManager.getInstance(MainActivity.this);
                            if (networkManager.isNetworkConnected()) {
                                getTokenDialog();
                            } else {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle(getString(R.string.mainActivity_dialog_noInternet_title))
                                        .setMessage(R.string.mainActivity_dialog_noInternet_message)
                                        .setIcon(android.R.drawable.stat_sys_warning)
                                        .setPositiveButton(getString(R.string.okButton), null)
                                        .show();
                            }
                        } catch (Exception e) {
                            Log.e("MAIN_ACCESSBTN", e.getMessage());
                        }

                    }
                }

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQ) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            } else {
                Functions.toast(MainActivity.this, "App needs media permissions to function!", true);
            }
        }
    }

    public static void showDialog() {
        mProgressDialog = new ProgressDialogCreator(Constants.APP_CONTEXT, Constants.APP_CONTEXT.getString(
                R.string.databaseManagerService_dialog_title_downloading), Constants.APP_CONTEXT.getString(R.string.databaseManagerService_dialog_text_downloading));
        mProgressDialog.showDialog();
        progressDialogShowing = true;
    }

    public void setProgressDialogContent(int currentStep) {

        switch (currentStep) {
            case DatabaseManagerService.TAG_STEP_DOWNLOADING: {
                mProgressDialog.setTitle(getResources().getString(R.string.databaseManagerService_dialog_title_downloading));
                mProgressDialog.setMessage(getResources().getString(R.string.databaseManagerService_dialog_text_downloading));
                break;
            }
            case DatabaseManagerService.TAG_STEP_UNZIPPING: {
                mProgressDialog.setTitle(getResources().getString(R.string.databaseManagerService_dialog_title_unzipping));
                mProgressDialog.setMessage(getResources().getString(R.string.databaseManagerService_dialog_text_unzipping));
                break;
            }
            case DatabaseManagerService.TAG_STEP_IMPORTING: {
                mProgressDialog.setTitle(getResources().getString(R.string.databaseManagerService_dialog_title_importing));
                mProgressDialog.setMessage(getResources().getString(R.string.databaseManagerService_dialog_text_importing));
                break;
            }
        }

        mProgressDialog.showDialog();
    }

    /**
     * Dialog for token input, get a token from the user and start to download/update the database
     */
    private void getTokenDialog() {

        final Dialog tokenDialog = new Dialog(MainActivity.this);
        tokenDialog.setContentView(R.layout.dialog_token);
        tokenDialog.setTitle(R.string.secretToken_dialog_title);

        final EditText editText_token = (EditText) tokenDialog.findViewById(R.id.editText_token);

        Button button_confirmToken = (Button) tokenDialog.findViewById(R.id.button_confirmToken);
        button_confirmToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String token = editText_token.getText().toString();

                    if (!token.isEmpty()) {
                        StringBuilder url = new StringBuilder();
                        url.append(Constants.SITE_URL);
                        url.append("home/export?token=");
                        url.append(token);

                        new RequestAsyncTask(MainActivity.this).execute(url.toString());
                        current_token = token;
                    } else {
                        Functions.toast(MainActivity.this, getString(R.string.secretToken_dialog_text), false);
                    }

                    tokenDialog.dismiss();
                } catch (Exception e) {
                    Log.e("MAIN_CONFIRM_TOKEN", e.getMessage());
                }

            }
        });
        tokenDialog.show();
    }

    @Override
    public void onTaskCompleted(String response) {
        Log.d("MAIN ON TASK 100%", response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("response").equals("Invalid token")) {
                Functions.toast(MainActivity.this, getString(R.string.secretToken_dialog_text), false);
            } else {
                Functions.writeStringToPrefs(MainActivity.this, Constants.PREF_SECRET_TOKEN_KEY,
                        current_token); // on API response, if the tokes is valid, save it in prefs
                showDialog();
                // start service
                Intent databaseManagerIntent = new Intent(MainActivity.this, DatabaseManagerService.class);
                databaseManagerIntent.putExtra(DatabaseManagerService.TAG_RECEIVER,
                        new DownloadReceiver(new Handler()));
                databaseManagerIntent.putExtra(DatabaseManagerService.TAG_DATABASE_MANAGER_TYPE,
                        DatabaseManagerService.DOWNLOAD_DATABASE_TYPE);
                startService(databaseManagerIntent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setLanguage() {

        mLocaleLanguageChanger = LocaleLanguageChanger.getInstance();
        mLocaleLanguageChanger.setLanguageAtStart(MainActivity.this);

    }

    /**
     * Check if the screen size is equal or bigger than 6.5
     *
     * @param context
     */
    public static void isDeviceTablet(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        float yInches = metrics.heightPixels / metrics.ydpi;
        float xInches = metrics.widthPixels / metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
        if (diagonalInches < 6.5) {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setCancelable(false);
            alert.setTitle(context.getString(R.string.mainActivity_dialog_screenDevice_title));
            alert.setMessage(context.getString(R.string.mainActivity_dialog_screenDevice_message));
            alert.setPositiveButton(R.string.okButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    System.exit(0);
                }
            });
            alert.show();
        }
    }

    /**
     * This class receive data from Service and update the progress dialog
     */
    public static class DownloadReceiver extends ResultReceiver {

        private Parcelable.Creator CREATOR = null;

        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if (mProgressDialog == null) {
                return;
            }

            switch (resultCode) {
                case DatabaseManagerService.UPDATE_PROGRESS_CODE: {
                    int progress = resultData.getInt(DatabaseManagerService.TAG_PROGRESS);
                    mProgressDialog.setProgress(progress);
                    break;
                }
                case DatabaseManagerService.UPDATE_DIALOG_STEP: {
                    int step = resultData.getInt(DatabaseManagerService.TAG_STEP);
                    switch (step) {
                        case DatabaseManagerService.TAG_STEP_DOWNLOADING: {
                            mProgressDialog.setTitle(Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_title_downloading));
                            mProgressDialog.setMessage(Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_text_downloading));
                            break;
                        }
                        case DatabaseManagerService.TAG_STEP_UNZIPPING: {
                            mProgressDialog.setTitle(Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_title_unzipping));
                            mProgressDialog.setMessage(Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_text_unzipping));
                            break;
                        }
                        case DatabaseManagerService.TAG_STEP_IMPORTING: {
                            mProgressDialog.setTitle(Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_title_importing));
                            mProgressDialog.setMessage(Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_text_importing));
                            break;
                        }
                        case DatabaseManagerService.TAG_STEP_GENERATING_THUMBNAILS: {
                            mProgressDialog.setTitle(Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_title_generating_thumbs));
                            mProgressDialog.setMessage( Constants.APP_CONTEXT.getResources().getString(R.string.databaseManagerService_dialog_text_generating_thumbs));
                            break;
                        }
                    }
                    break;
                }
//                case DatabaseManagerService.UPDATE_DATABASE_TYPE: {
//                    mProgressDialog.showDialog();
//                    break;
//                }
                case DatabaseManagerService.DISMISS_DIALOG_CODE: {
                    if(mProgressDialog != null) {
                        mProgressDialog.dismissDialog();
                        progressDialogShowing = false;
                    }
                    break;
                }
            }
        }
    }

}
