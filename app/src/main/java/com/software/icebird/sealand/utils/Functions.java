package com.software.icebird.sealand.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.StatFs;
import android.widget.Toast;

import com.software.icebird.sealand.DatabaseHelper;

import java.io.File;
import java.util.Set;

/**
 * Created by catalin on 8/16/16.
 */
public class Functions {

    /**
     * Set text font to elements
     * @param context - used to get font from assets
     * @return Typeface object: myTextView.setTypeface(typeface)
     */
    public static Typeface getTextFont(Context context) {

        AssetManager assetManager = context.getApplicationContext().getAssets();
        return Typeface.createFromAsset(assetManager, Constants.TEXT_FONT_NAME);
    }

    /**
     *
     * @return long bytes available
     */
    public static long getInternalSpaceAvailable() {
        File file = Environment.getDataDirectory();
        StatFs stat = new StatFs(file.getPath());
        long bytesAvailable = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        } else {
            bytesAvailable = stat.getBlockSize() * stat.getAvailableBlocks();
        }
        return bytesAvailable;
    }

    /**
     *
     * @param context
     * @param key unique string key value
     * @param value int value stored
     */
    public static void writeIntToPrefs(Context context, String key, int value){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int readIntFromPrefs(Context context, String key, int defaultValue){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPref.getInt(key, defaultValue);
    }

    public static void writeStringToPrefs(Context context, String key, String value){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String readStringFromPrefs(Context context, String key, String defaultValue){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);

        return sharedPref.getString(key, defaultValue);
    }

    public static boolean isInSharePreference(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.contains(key);
    }

    /**
     *
     * @param context
     * @param key unique string key value
     * @param value set of strings
     */
    public static void writeStringSetToPrefs(Context context, String key, Set value){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet(key, value);
        editor.commit();
    }

    public static Set readStringSetFromPrefs(Context context, String key, Set defaultValue){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPref.getStringSet(key, defaultValue);
    }

    public static void logOutUser(Context context){
        writeStringToPrefs(context, Constants.PREF_LOGGED_IN_MEMBER_EMAIL, null);
        writeStringToPrefs(context, Constants.PREF_OFFERS_JSON, null);
    }

    public static void clearAllSharedPrefsData(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }

    public static int getSelectedBrand(Context context) {
        return readIntFromPrefs(context, Constants.PREF_BRAND, 0);
    }

    public static void toast(Context context, String text, boolean longToast){
        Toast.makeText(context, text, longToast ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }

    /**
     * All user input must be escaped before running a query with it !!
     * @param statement SQL statement to be escaped
     * @return escaped SQL
     */
    public static String escapeSQL(String statement) {
        return DatabaseUtils.sqlEscapeString(statement);
    }

    /**
     *
     * @param statement SQL Statement to be executed, eg "SELECT * FORM Pump WHERE ID = 1;"
     * @param getColumn Column to be returned, eg "Title"
     * @return
     */
    public static String execSQLGetFirstString(String statement, String getColumn) {
        //statement = DatabaseUtils.sqlEscapeString(statement);
        SQLiteDatabase databaseHelper = DatabaseHelper.getDatabaseHelper(
                Constants.APP_CONTEXT).getWritableDatabase();
        Cursor cursor = databaseHelper.rawQuery(statement, null);
        cursor.moveToFirst();
        try {
            return cursor.getString(cursor.getColumnIndex(getColumn));
        } catch (CursorIndexOutOfBoundsException cursorIndexEx) {
            return null;
        } finally {
            cursor.close();
        }
    }

    /**
     *
     * @param statement SQL Statement to be executed, eg "SELECT * FORM Pump WHERE ID = 1;"
     * @param getColumn Column to be returned, eg "Title"
     * @return
     */
    public static int execSQLGetFirstInt(String statement, String getColumn) {
        //statement = DatabaseUtils.sqlEscapeString(statement);
        SQLiteDatabase databaseHelper = DatabaseHelper.getDatabaseHelper(
                Constants.APP_CONTEXT).getWritableDatabase();
        Cursor cursor = databaseHelper.rawQuery(statement, null);
        cursor.moveToFirst();
        try {
            return cursor.getInt(cursor.getColumnIndex(getColumn));
        } catch (CursorIndexOutOfBoundsException cursorIndexEx) {
            return -1;
        } finally {
            cursor.close();
        }
    }

    /**
     *
     * @param statement SQL Statement to be executed, eg "SELECT * FORM Pump WHERE ID = 1;"
     * @return cursor
     */
    public static Cursor execSQLGetCursor(String statement) {
        //statement = DatabaseUtils.sqlEscapeString(statement);
        SQLiteDatabase databaseHelper = DatabaseHelper.getDatabaseHelper(
                Constants.APP_CONTEXT).getWritableDatabase();
        Cursor cursor = databaseHelper.rawQuery(statement, null);
        return cursor;
    }

    /**
     *
     * @param plainTextPassword password to be hashed
     * @param salt hashing salt
     * @return method + salt + hash
     *
     * Example:
     * String plainTextPass = "demo123";
     * String method = "$2a$";
     * String salt = "10$032d889a15a949684a2ff1";
     * String hash = "$2y$10$032d889a15a949684a2ffuv6b55tRX2lM.f6eTFnnwmkw5gKy4N2y";
     */
    public static String hashPassword(String plainTextPassword, String salt) {
        String method = "$2a$"; //BCrypt blowfish
        return BCrypt.hashpw(plainTextPassword, method + salt);
    }

    /**
     *
     * @param originalHash
     * @param plainTextPassword
     * @param salt
     * @return True if hashes match, else false
     */
    public static boolean hashPasswordAssertEqualsHash(String originalHash, String plainTextPassword, String salt) {
        return originalHash.substring(3).equals(hashPassword(plainTextPassword, salt).substring(3));
    }

    /**
     * Loading bitmaps efficiently
     * https://developer.android.com/training/displaying-bitmaps/load-bitmap.html
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String filePath, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    public static double convertFlow(double flow, String convertToUnit) {
        switch (convertToUnit) {
            case "l/min": {
                flow /= 16.666667;
                break;
            }
            case "l/s": {
                flow /= 0.277778;
                break;
            }
            case "m3/h": {
                // Divided by 1
                break;
            }
            case "US g.p.m.": {
                flow /= 4.402868;
                break;
            }
            case "Imp g.p.m.": {
                flow /= 3.666154;
                break;
            }
            case "ft3/min": {
                flow /= 0.588578;
                break;
            }
            case "m3/min": {
                flow /= 0.016667;
                break;
            }
            case "ft3/h": {
                flow /= 35.314667;
                break;
            }
        }
        flow = (double) Math.round(flow * 10) / 10;
        return flow;
    }

    public static double convertHead(double head, String convertToUnit) {
        switch (convertToUnit) {
            case "m": {
                //Divided by 1
                break;
            }
            case "ft": {
                head *= 3.280839;
                break;
            }
            case "yd": {
                head *= 1.093613;
                break;
            }
            case "fath": {
                head *= 0.546806;
                break;
            }
        }
        head = (double) Math.round(head * 10) / 10; // 1 digit after .
        return head;
    }



}

