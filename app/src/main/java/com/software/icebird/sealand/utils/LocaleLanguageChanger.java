package com.software.icebird.sealand.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import java.util.Locale;

/**
 * Created by ch on 27.09.2016.
 */

public class LocaleLanguageChanger {

    private static LocaleLanguageChanger localeLanguageChanger;

    private String[] languageArray = {"en", "it", "fr", "es"};

    private static int INDEX_LANGUAGE;

    private static final String SELECTED_LANGUAGE = "SELECTEDLANGUAGE";

    private LocaleLanguageChanger() {
        INDEX_LANGUAGE = -1;
    }

    public static LocaleLanguageChanger getInstance() {
        if(localeLanguageChanger == null) {
            localeLanguageChanger = new LocaleLanguageChanger();
        }

        return localeLanguageChanger;
    }

    public void setDeviceLanguage(Context context, String language) {
        Locale locale = new Locale(language);

        Resources resources = context.getResources();

        DisplayMetrics displayMetrics = resources.getDisplayMetrics();

        Configuration configuration = resources.getConfiguration();

        configuration.locale = locale;

        resources.updateConfiguration(configuration, displayMetrics);

        setLanguageInSharePreference(context, language);

        INDEX_LANGUAGE = getLanguageIndex(language);
    }

    /**
     * Get device language
     * @param context
     * @return - en, it, fr, es
     */
    private String getDeviceLanguage(Context context) {
        return Locale.getDefault().getLanguage();
    }

    private void setLanguageInSharePreference(Context context, String language) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(SELECTED_LANGUAGE, language);
        editor.apply();
    }

    public String getLanguageFromSharePreference(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        if(sharedPreferences.contains(SELECTED_LANGUAGE)) {
            return sharedPreferences.getString(SELECTED_LANGUAGE, getDeviceLanguage(context));
        } else {
            return "";
        }
    }

    /**
     * Check if device language is in list, if not return the ENGLISH
     * language by default
     * @param language - device language
     * @return - default language english
     */
    private int getLanguageIndex(String language) {
        for(int i = 0; i < languageArray.length; i++) {
            if(languageArray[i].equals(language)) {
                return i;
            }
        }

        return 0;
    }

    private boolean isLanguageAvailable(String language) {
        for(String lang : languageArray) {
            if(lang.equals(language)) {
                return true;
            }
        }

        return false;
    }

    public void setLanguageAtStart(Context context) {
        String deviceLanguage = getDeviceLanguage(context);

        String languageFromSharePref = getLanguageFromSharePreference(context);

        if(!languageFromSharePref.equals("")) {
            setDeviceLanguage(context, languageFromSharePref);
        } else {
            if(isLanguageAvailable(deviceLanguage)) {
                setDeviceLanguage(context, deviceLanguage);
            } else {
                setDeviceLanguage(context, "en");
            }
        }
    }

    public static int getIndexLanguage() {
        return INDEX_LANGUAGE;
    }
}
