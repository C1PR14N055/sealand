package com.software.icebird.sealand;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by ciprian on 8/16/16.
 */
public class NetworkManager {

    private static Context mContext = null;

    /**
     * Singleton, no need for more than one network manager
     */
    private static NetworkManager networkManager = new NetworkManager();
    private NetworkManager() {}

    public static NetworkManager getInstance(Context context) {
        mContext = context;
        return networkManager;
    }

    /**
     * Network connected does not mean there is internet too!
     */
    public boolean isNetworkConnected() {

        try{
            ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            boolean isConnected = networkInfo != null &&
                    networkInfo.isConnectedOrConnecting();
            return isConnected;
        } catch (Exception e) {
            Log.e("NETWORK_MANAGER", e.getMessage());
        }

        return false;
    }

    /* MEH
    public boolean isInternetAvailable() {
        boolean success = false;
        try {
            URL url = new URL(Constants.SITE_URL); // ping site
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(5000); // 5s timeout
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
    */
}
