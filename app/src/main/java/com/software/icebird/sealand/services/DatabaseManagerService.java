package com.software.icebird.sealand.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.ResultReceiver;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.software.icebird.sealand.DatabaseHelper;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.activities.MainActivity;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Created by catalin on 8/22/16.
 */
public class DatabaseManagerService extends IntentService {

    //Tell service to download entire DB or just an update
    public static final String TAG_DATABASE_MANAGER_TYPE = "database_manager_type";
    public static final String TAG_FILE_ID = "file_id";
    public static final String TAG_RECEIVER = "receiver";
    public static final int DOWNLOAD_DATABASE_TYPE = 0;
    public static final int UPDATE_DATABASE_TYPE = 3;

    //used to change download/unzip/import dialog
    public static final int DISMISS_DIALOG_CODE = -1;
    public static final int UPDATE_PROGRESS_CODE = 1;
    public static final String TAG_PROGRESS = "progress";
    public static final int UPDATE_DIALOG_STEP = 2;
    public static final String TAG_STEP = "step";
    public static final int TAG_STEP_DOWNLOADING = 3;
    public static final int TAG_STEP_UNZIPPING = 4;
    public static final int TAG_STEP_IMPORTING = 5;
    public static final int TAG_STEP_GENERATING_THUMBNAILS = 6;

    // notif id
    public static final int NOTIF_ID = 6;

    public static int sCurrentStep = -1;

    private boolean downloadDone = false;
    private boolean unzipDone = false;
    private boolean importDone = false;

    private String appSavePath = "";

    private ResultReceiver mResultReceiver;

    public DatabaseManagerService() {
        super("DatabaseManagerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        int operationType = intent.getIntExtra(TAG_DATABASE_MANAGER_TYPE, 0);
        mResultReceiver = intent.getParcelableExtra(TAG_RECEIVER);
        appSavePath = Constants.APP_CONTEXT.getFilesDir().getPath();

        PowerManager mgr = (PowerManager) Constants.APP_CONTEXT.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "wakeLock");
        wakeLock.acquire();

        Log.d("WakeLock", "acquired");

        switch (operationType) {
            case DOWNLOAD_DATABASE_TYPE: {
                downloadEntireDB();
                unZipFile(appSavePath + "/db_main.zip", appSavePath);
                Log.d("UNZIPPING IN", appSavePath);
                importDatabase();
                generateThumbnails();
                dismissProgressDialog();
                if (downloadDone && unzipDone && importDone) {
                    Functions.writeStringToPrefs(Constants.APP_CONTEXT, Constants.PREF_INITIAL_DB_INSTALLED_KEY,
                            Constants.PREF_INITIAL_DB_INSTALLED_KEY);
                    notifyUser(true);
                    System.exit(0);
                }
                break;
            }

            case UPDATE_DATABASE_TYPE: {
                String fileId = intent.getStringExtra(TAG_FILE_ID);
                downloadUpdate(fileId);
                unZipFile(appSavePath + "/db_update.zip", appSavePath);
                importDatabase();
                generateThumbnails();
                notifyUser(false);
                System.exit(0);

                break;
            }
        }

        wakeLock.release();
        Log.d("WakeLock", "released");

    }

    /**Notify user if is about finish install or finish update
     *
     * @param isDownload true - for install
     *                   false - for update
     */
    private void notifyUser(boolean isDownload){
        try{
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this);

            mBuilder.setSmallIcon(R.drawable.logo);
            mBuilder.setContentTitle(getString(R.string.databaseManagerService_dialog_title_appName));
            mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
            mBuilder.setSound(uri);

            if(isDownload) {
                mBuilder.setContentText(getString(R.string.databaseManagerService_dialog_text_appInstalled));
            } else {
                mBuilder.setContentText(getString(R.string.databaseManagerService_dialog_text_appUpdated));
            }

            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(Constants.APP_CONTEXT, MainActivity.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(MainActivity.class);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(DatabaseManagerService.NOTIF_ID , mBuilder.build());
        } catch (Exception e) {
            Log.e("DATABASE_NOTYF_USER", e.getMessage());
        }
    }

    /**
     *Initialize download for file
     */
    public void downloadEntireDB(){
        //Log.i("PATH", Environment.getExternalStorageDirectory().getPath());

        StringBuilder url = new StringBuilder();
        url.append(Constants.SITE_URL);
        url.append("home/export?token=");
        url.append(Functions.readStringFromPrefs(Constants.APP_CONTEXT, Constants.PREF_SECRET_TOKEN_KEY, ""));
        url.append("&download=true");

        downloadFile(url.toString(), appSavePath, "/db_main.zip");
    }

    /**
     * Initialize update for file
     * @param fileId update fileID
     */
    public void downloadUpdate(String fileId) {

        StringBuilder url = new StringBuilder();
        url.append(Constants.SITE_URL);
        url.append("home/export?token=");
        url.append(Functions.readStringFromPrefs(Constants.APP_CONTEXT, Constants.PREF_SECRET_TOKEN_KEY, ""));
        url.append("&file_id=");
        url.append(fileId);
        url.append("&download=true");

        downloadFile(url.toString(), appSavePath, "/db_update.zip");
    }

    /**
     * Download file from a url
     * @param url - url file location
     * @param savePath - location on device memory
     * @param saveName - file name
     */
    public void downloadFile(final String url, final String savePath, final String saveName) {

        try {

            updateProgressDialogStep(TAG_STEP_DOWNLOADING);

            URL sourceUrl = new URL(url);
            URLConnection conn = sourceUrl.openConnection();
            conn.connect();
            InputStream inputStream = conn.getInputStream();

            int fileSize = conn.getContentLength();

            File saveFilePath = new File(savePath);
            if (!saveFilePath.exists()) {
                saveFilePath.mkdirs();
            }
            File saveFile = new File(savePath + saveName);
            if (saveFile.exists()) {
                saveFile.delete();
            }
            saveFile.createNewFile();

            FileOutputStream outputStream = new FileOutputStream(
                    savePath + saveName, true);

            byte[] buffer = new byte[1024];
            long readCount = 0;
            int readNum = 0;
            int prevPercent = 0;
            while (readCount < fileSize && readNum != -1) {
                readNum = inputStream.read(buffer);
                if (readNum > -1) {
                    outputStream.write(buffer, 0, readNum);

                    readCount = readCount + readNum;

                    int percent = (int) (readCount * 100 / fileSize);
                    if (percent > prevPercent) {
                        updateProgressDialogPercent(percent);
                        prevPercent = percent;
                    }
                }
            }
            outputStream.flush();
            outputStream.close();
            inputStream.close();
            downloadDone = true;

        } catch (Exception e) {
            dismissProgressDialog();
            Log.d("FILE_DOWNLOAD_ERROR", e.getMessage());
            downloadDone = false;
        }

    }

    /**
     * Unzip downloaded file and after unzip, delete the zip file.
     * @param zipFilePath - full path of file which you want to unzip
     * @param extractLocationPath - extractLocationPath of unzip content
     */
    public void unZipFile(String zipFilePath, String extractLocationPath) {

        int size;
        byte[] buffer = new byte[1024];

        try {
            if ( !extractLocationPath.endsWith("/") ) {
                extractLocationPath += "/";
            }

            Log.i("UNZIP", "Start");
            updateProgressDialogStep(TAG_STEP_UNZIPPING);

            File f = new File(extractLocationPath);
            if(!f.isDirectory()) {
                f.mkdirs();
            }

            int totalZipEntries = new ZipFile(zipFilePath).size();
            int writtenZipEntries = 0;
            Log.d("ZIP FILE PATH", zipFilePath);

            ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFilePath), 1024));

            try {
                ZipEntry ze;

                while ((ze = zin.getNextEntry()) != null) {
                    String path = extractLocationPath + ze.getName();
                    File unzipFile = new File(path);

                    writtenZipEntries++;
                    Log.d("WRITTEN", String.valueOf(writtenZipEntries) + "/" + String.valueOf(totalZipEntries));
                    int percent = writtenZipEntries * 100 / totalZipEntries;
                    updateProgressDialogPercent(percent);

                    if (ze.isDirectory()) {
                        if(!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        // check for and create parent directories if they don't exist
                        File parentDir = unzipFile.getParentFile();
                        if ( null != parentDir ) {
                            if ( !parentDir.isDirectory() ) {
                                parentDir.mkdirs();
                            }
                        }

                        // unzip the file
                        FileOutputStream fos = new FileOutputStream(unzipFile, false);
                        BufferedOutputStream bos = new BufferedOutputStream(fos, 1024);

                        try {
                            while ((size = zin.read(buffer, 0, 1024)) != -1 ) {
                                bos.write(buffer, 0, size);

                            }
                            zin.closeEntry();
                        }
                        finally {
                            bos.flush();
                            bos.close();
                        }
                    }
                }
            }
            finally {
                zin.close();
            }

            Log.i(Constants.DEBUG_TAG, "UNZIP Finished");

            File zipFile = new File(zipFilePath);
            if(zipFile.exists()) {
                zipFile.delete();
                Log.i(Constants.DEBUG_TAG, "DELETED_ZIP_FILE");
            }
            unzipDone = true;
        }
        catch (Exception e) {
            dismissProgressDialog();
            Log.e(Constants.DEBUG_TAG, "Unzip exception", e);
            unzipDone = false;
        }

    }

    /**
     * Get all path from a directory
     * @param parentDirectory - directory of files that are taken
     * @return - List with file from directory
     */
    public static List<File> getAllFilesInPath(File parentDirectory) {

        List<File> inFileList = new ArrayList<>();

        File[] files = parentDirectory.listFiles();
        for(File file : files) {
            if(file.isDirectory()) {
                inFileList.addAll(getAllFilesInPath(file));
            } else {
                inFileList.add(file);
            }
        }

        return inFileList;
    }

    public void importDatabase() {

        updateProgressDialogStep(TAG_STEP_IMPORTING);
        String path = appSavePath + "/db.sql";

        try {
            insertInDbFromFile(Constants.APP_CONTEXT, path);
            importDone = true;
        } catch (IOException e) {
            Functions.toast(Constants.APP_CONTEXT, e.toString(), false);
            importDone = false;
            e.printStackTrace();
        }
    }

    /**
     *
     * @param context app context
     * @param path resource of raw file to read and execute as SQL statements
     * @throws IOException
     *
     * Read whole SQL file into a String, splits the string by ; and executes the SQL
     *
     */

    public void insertInDbFromFile(Context context, String path) throws IOException {

        SQLiteDatabase databaseHelper = DatabaseHelper.getDatabaseHelper(
                Constants.APP_CONTEXT).getWritableDatabase();

        // Open the resource

        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
        int totalSqlLines = 0;
        while (bufferedReader.readLine() != null) totalSqlLines++;
        bufferedReader.close();
        Log.d("total lines", String.valueOf(totalSqlLines));

        bufferedReader = new BufferedReader(new FileReader(path)); //reopen buffered reader for SQL read

        int crtLineNr = 0;

        StringBuilder total = new StringBuilder();

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            total.append(line + "\n");
            crtLineNr++;
            if (crtLineNr % 10 == 0) Log.d("CRT LINE", String.valueOf(crtLineNr));
            if (total.indexOf(";") != -1) {
                try {
                    databaseHelper.execSQL(total.substring(0, total.indexOf(";") + 1));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                //Log.d("EXEC SQL", total.substring(0, total.indexOf(";") + 1));
                total.delete(0, total.indexOf(";") + 1);
                if (crtLineNr % 100 == 0) updateProgressDialogPercent(crtLineNr * 100 / totalSqlLines); // % 100 to not overkill UI thread
            }
        }

        updateProgressDialogPercent(100);
        File file = new File(path);
        file.delete();
        Log.d("DEL", path);
    }

    private void generateThumbnails() {

        try {
            updateProgressDialogStep(TAG_STEP_GENERATING_THUMBNAILS);
            Cursor cursor = Functions.execSQLGetCursor("SELECT * FROM File WHERE ClassName = \"Image\" " +
                    "AND Filename LIKE \"assets/Data/Picture/%\";");
            int nrTotalImgs = cursor.getCount();
            File file = new File(Constants.APP_CONTEXT.getFilesDir() + "/" + Constants.THUMBS_DIR);
            if (!file.isDirectory()) {
                file.mkdirs();
            }
            Log.d("DIR EXISTSSS", String.valueOf(file.isDirectory()));
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) Constants.APP_CONTEXT.getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(metrics);
            int requiredWidthForDevice = metrics.widthPixels / 4;
            FileOutputStream fos = null;
            for (int i = 0; i < nrTotalImgs; i++) {
                updateProgressDialogPercent(i * 100 / nrTotalImgs);
                cursor.moveToPosition(i);
                File testFile = new File(Constants.APP_CONTEXT.getFilesDir().getPath()
                        + "/" + cursor.getString(cursor.getColumnIndex("Filename")));
                if (!testFile.exists()) {
                    Log.w("NO SUCH FILE", cursor.getString(cursor.getColumnIndex("Filename")));
                    continue;
                }
                Bitmap bitmap = Functions.decodeSampledBitmapFromResource(Constants.APP_CONTEXT.getFilesDir().getPath()
                        + "/" + cursor.getString(cursor.getColumnIndex("Filename")), requiredWidthForDevice, requiredWidthForDevice);
                try {
                    file = new File(Constants.APP_CONTEXT.getFilesDir().getPath() + "/" +
                            Constants.THUMBS_DIR, cursor.getString(cursor.getColumnIndex("Title")));
                    fos = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                finally {
                    if (fos != null) {
                        try {
                            fos.flush();
                            fos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                Log.d("THUMB", i + "/" + (nrTotalImgs - 1));
            }
        } catch (Exception e) {
            Log.e("DATABASE_GENERATETHUMB", e.getMessage());
        }
    }

    private void updateProgressDialogPercent(int percent) {
        Bundle resultData = new Bundle();
        resultData.putInt(TAG_PROGRESS, percent);
        mResultReceiver.send(UPDATE_PROGRESS_CODE, resultData);
    }

    private void updateProgressDialogStep(int step) {

        sCurrentStep = step; //keep step type to remake the progress dialog

        updateProgressDialogPercent(0); // reset count when step changes
        Bundle resultData = new Bundle();
        resultData.putInt(TAG_STEP, step);
        mResultReceiver.send(UPDATE_DIALOG_STEP, resultData);
    }

    private void dismissProgressDialog() {
        Bundle resultData = new Bundle();
        mResultReceiver.send(DISMISS_DIALOG_CODE, resultData);
    }
}