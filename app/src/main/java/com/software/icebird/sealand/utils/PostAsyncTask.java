package com.software.icebird.sealand.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.software.icebird.sealand.Offer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by C on 05/10.
 */

public class PostAsyncTask extends AsyncTask<Offer, Void, String> {

    private final PostAsyncTask.OnPostTaskCompleted taskCompleted;

    public PostAsyncTask(PostAsyncTask.OnPostTaskCompleted activityContext){
        this.taskCompleted = activityContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Offer... params) {
        Offer offer = params[0]; // URL to call
        URL postURL = null;
        try {
            postURL = new URL(Constants.SITE_URL + "api/Offer");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Map<String, String> comment = new HashMap<>();
        //no ID needed
        comment.put("Name", offer.name);
        comment.put("Company", offer.company);
        comment.put("Address", offer.address);
        comment.put("Telephone", offer.phone);
        comment.put("Fax", offer.fax);
        comment.put("Email", offer.email);
        comment.put("Reference", offer.reference);
        comment.put("OfferNo", offer.id);
        comment.put("Pump", String.valueOf(offer.pumpID));
        comment.put("PDF", offer.pdfJsonID);
        String userEmail = Functions.readStringFromPrefs(Constants.APP_CONTEXT,
                Constants.PREF_LOGGED_IN_MEMBER_EMAIL, "");
        String userID = Functions.execSQLGetFirstString("SELECT * FROM Member WHERE Email = " + userEmail, "ID");
        comment.put("MemberID", userID);
        String json = new GsonBuilder().create().toJson(comment, Map.class);

        //String json = "{\"ID\":11,\"Sort\":\"0\",\"Name\":\"Alin Lup\",\"Company\":\"test\",\"Address\":\"test\",
        // \"Telephone\":\"2134351\",\"Fax\":\"3213213\",\"Email\":\"alin.lup@welltech.ro\",\"Reference\":null,\
        // "OfferNo\":\"0\",\"Pump\":12,\"Member\":56,\"PDF\":1337}";

        String respString = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) postURL.openConnection();

            // Set Headers
            String token = Functions.readStringFromPrefs(Constants.APP_CONTEXT,
                    Constants.PREF_SECRET_TOKEN_KEY, "");
            conn.setRequestProperty("Token", token);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            osw.write(json.toString());
            osw.flush();
            osw.close();

            conn.connect();

            // Request not successful
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                //throw new RuntimeException("Request Failed. HTTP Error Code: " + conn.getResponseCode());
                Functions.toast(Constants.APP_CONTEXT, "Request Failed. HTTP Error Code: " +
                        conn.getResponseCode(), true);
            }

            Log.e("HTTPPOST", conn.getResponseCode() + "");

            // Read response
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                respString += line;
            }
            br.close();
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return respString;

    }


    @Override
    protected void onPostExecute(String respString) {
        taskCompleted.onPostTaskCompleted(respString); //call onTaskCompleted
    }

    public interface OnPostTaskCompleted {
        void onPostTaskCompleted(String response); //callback MainActivity
    }
}