package com.software.icebird.sealand.utils;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by ciprian on 8/25/16.
 */
public class RequestAsyncTask extends AsyncTask<String, Void, JSONObject> {

    private OnTaskCompleted taskCompleted;

    public RequestAsyncTask(OnTaskCompleted activityContext){
        this.taskCompleted = activityContext;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        try{

            URL url = new URL(strings[0]);
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(5000);
            connection.connect();

            InputStream receivedStream = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(receivedStream));
            StringBuilder responseJsonString = new StringBuilder();

            String line;
            while((line = reader.readLine()) != null) {
                responseJsonString.append(line);
            }

            //close streams
            receivedStream.close();
            reader.close();

            return new JSONObject(responseJsonString.toString());

        }catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        taskCompleted.onTaskCompleted(jsonObject.toString()); //call onTaskCompleted
    }

    public interface OnTaskCompleted {
        void onTaskCompleted(String response); //callback MainActivity
    }
}
