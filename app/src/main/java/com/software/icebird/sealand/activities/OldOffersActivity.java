package com.software.icebird.sealand.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.software.icebird.sealand.MenuManager;
import com.software.icebird.sealand.NetworkManager;
import com.software.icebird.sealand.Offer;
import com.software.icebird.sealand.R;
import com.software.icebird.sealand.interfaces.OnRefreshActivity;
import com.software.icebird.sealand.utils.Constants;
import com.software.icebird.sealand.utils.Functions;
import com.software.icebird.sealand.utils.LocaleLanguageChanger;
import com.software.icebird.sealand.utils.PostAsyncTask;
import com.software.icebird.sealand.utils.ProgressDialogCreator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OldOffersActivity extends AppCompatActivity implements OnRefreshActivity,
        PostAsyncTask.OnPostTaskCompleted, JSInterface.OnJSONPDFResponse {

    private Context mContext;
    private Gson mGson;
    Button syncButton, selectAllButton;
    TableLayout tl;
    WebView webViewHack;
    TextView backTextView;
    private static List<Integer> offerIndexesToSync;
    public static boolean isActive = false;
    private static Offer crtOffer = null;

    private static String serializedOffers;
    private static Offer[] offers;

    //progress dialog
    ProgressDialogCreator pushOffersDialog;
    private int totalNumberOfOfferToSync = 0;
    private int counterOfferSynchronized = 0;

    //keep screen active
//    private PowerManager.WakeLock keepWakeUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_offers);

        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
        if (NetworkManager.getInstance(mContext).isNetworkConnected()) {
            syncButton.setVisibility(View.VISIBLE);
        } else {
            syncButton.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isActive = true;
    }

    private void addIfNotThere(int num) {
        for (int el : offerIndexesToSync) {
            if (el == num) return;
        }
        offerIndexesToSync.add(num);
        Log.d("ADD", String.valueOf(num));
    }

    private void removeIfThere(int num) {
        for (int i = 0; i < offerIndexesToSync.size(); i++) {
            if (offerIndexesToSync.get(i) == num) {
                Log.d("REM", String.valueOf(offerIndexesToSync.get(i)));
                offerIndexesToSync.remove(i);
            }
        }
    }

    private void init() {

        //init Context in Constanst
        Constants.APP_CONTEXT = OldOffersActivity.this;

        mContext = OldOffersActivity.this;

        //init screen on
        //PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        //this.keepWakeUp = powerManager.newWakeLock(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, "LOCK_OFF");

        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mGson = new Gson();
        offerIndexesToSync = new ArrayList<>();

        //set back textView text
        backTextView = (TextView) findViewById(R.id.offers_textView_back);
        backTextView.setText("<< " + getString(R.string.offers_textView_back));
        //close the activity and go back
        backTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        selectAllButton = (Button) findViewById(R.id.offers_button_selectAll);
        selectAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tl != null) {

                    for (int i = 1; i < tl.getChildCount(); i++) {

                        TableRow tr = (TableRow) tl.getChildAt(i);

                        CheckedTextView checkedTextView = (CheckedTextView) tr.getChildAt(7);

                        if (checkedTextView != null) {
                            if (!checkedTextView.isChecked()) {

                                //add offer index to offerIndexesToSync
                                addIfNotThere(i - 1);

                                //change the checkBox status
                                checkedTextView.setChecked(true);
                            }
                        }
                    }
                }
            }
        });

        LinearLayout linearLayout_appMenu = (LinearLayout) findViewById(R.id.linearLayout_oldOffers_appMenu);
        webViewHack = (WebView) findViewById(R.id.webView_hack);

        MenuManager menuManager = new MenuManager(mContext, linearLayout_appMenu, this);
        menuManager.initializeComponents();
        syncButton = (Button) findViewById(R.id.button_sync);
        syncButton.setOnClickListener(syncButtonOnClickListener);

        serializedOffers = Functions.readStringFromPrefs(Constants.APP_CONTEXT,
                Constants.PREF_OFFERS_JSON, null);

        if (serializedOffers != null) {
            //Offer[] offers = mGson.fromJson(serializedOffers, Offer[].class);
            offers = mGson.fromJson(serializedOffers, Offer[].class);
            //TableLayout tl = (TableLayout) findViewById(R.id.activity_old_offers_tableLayout);
            tl = (TableLayout) findViewById(R.id.activity_old_offers_tableLayout);
            int index = 1; //must be 1 to skip table head row
            for (final Offer o : offers) {
                TableRow tr = new TableRow(this);
                tr.setGravity(Gravity.CENTER_VERTICAL);
                ViewGroup.LayoutParams layoutParams = new TableRow.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        1f);

                tr.setLayoutParams(layoutParams);
                if (index % 2 == 0)
                    tr.setBackgroundColor(mContext.getResources().getColor(R.color.menu_background));

                // create columns
                TextView textView_date = new TextView(this);
                textView_date.setText(o.date);
                textView_date.setGravity(1);
                textView_date.setLayoutParams(layoutParams);
                textView_date.setEllipsize(TextUtils.TruncateAt.MIDDLE);

                TextView textView_order = new TextView(this);
                textView_order.setText(o.id.substring(0, 4) + "..." +
                        o.id.substring(o.id.length() - 4, o.id.length()));
                textView_order.setGravity(1);
                textView_order.setLayoutParams(layoutParams);
                textView_order.setEllipsize(TextUtils.TruncateAt.MIDDLE);

                TextView textView_company = new TextView(this);
                textView_company.setText(o.company);
                textView_company.setGravity(1);
                textView_company.setLayoutParams(layoutParams);

                TextView textView_reference = new TextView(this);
                textView_reference.setText(o.reference);
                textView_reference.setGravity(1);
                textView_reference.setLayoutParams(layoutParams);

                TextView textView_name = new TextView(this);
                textView_name.setText(o.pumpName);
                textView_name.setGravity(1);
                textView_name.setLayoutParams(layoutParams);

                TextView textView_graph = new TextView(this);
                String graphAnchor = "<a href='" + o.pumpChartUrl + "'>" + getString(R.string.offers_linkAddress_graph) + "</a>";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    textView_graph.setText(Html.fromHtml(graphAnchor, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    textView_graph.setText(Html.fromHtml(graphAnchor));
                }
                Log.d("GRAPH URL", graphAnchor);
                textView_graph.setMovementMethod(LinkMovementMethod.getInstance());
                textView_graph.setGravity(1);
                textView_graph.setLayoutParams(layoutParams);

                TextView textView_pdf = new TextView(this);
                String pdfAnchor = "<a href='" + o.pumpPdfPath + "'>" + getString(R.string.offers_linkAddress_viewPDF) + "</a>";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    textView_pdf.setText(Html.fromHtml(pdfAnchor, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    textView_pdf.setText(Html.fromHtml(pdfAnchor));
                }
                Log.d("PDF URL", pdfAnchor);
                textView_pdf.setMovementMethod(LinkMovementMethod.getInstance());
                textView_pdf.setGravity(1);
                textView_pdf.setLayoutParams(layoutParams);

                final CheckedTextView checkedTextView_synced = new CheckedTextView(this);
                checkedTextView_synced.setText(o.synced ? getString(R.string.offers_textView_syncStatus) : getString(R.string.offers_textView_notSyncStatus));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    checkedTextView_synced.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
                checkedTextView_synced.setEnabled(!o.synced);
                checkedTextView_synced.setChecked(o.synced);
                TypedValue value = new TypedValue();
                mContext.getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true);
                int checkMarkDrawableResId = value.resourceId;
                checkedTextView_synced.setCheckMarkDrawable(checkMarkDrawableResId);
                checkedTextView_synced.setGravity(Gravity.CENTER_VERTICAL);
                checkedTextView_synced.setLayoutParams(layoutParams);
                final int finalIndex = index;
                checkedTextView_synced.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkedTextView_synced.isEnabled()) {
                            checkedTextView_synced.setChecked(!checkedTextView_synced.isChecked());
                            /*Functions.toast(mContext, String.valueOf(finalIndex - 1) + " : " +
                                    String.valueOf(checkedTextView_synced.isChecked()), false);*/

                            if (checkedTextView_synced.isChecked()) {
                                addIfNotThere(finalIndex - 1);
                            } else {
                                removeIfThere(finalIndex - 1);
                            }
                        }
                    }
                });

                /* Add to row. */
                tr.addView(textView_date);
                tr.addView(textView_order);
                tr.addView(textView_company);
                tr.addView(textView_reference);
                tr.addView(textView_name);
                tr.addView(textView_graph);
                tr.addView(textView_pdf);
                tr.addView(checkedTextView_synced);
                tr.setPadding(0, dp2px(8), 0, dp2px(8));

                /* Add row to TableLayout. */
                tl.addView(tr, index, layoutParams);
                index++;
            }
        } else {
            Functions.toast(Constants.APP_CONTEXT, getString(R.string.offers_text_noOffersSaved), true);
        }

        //progress dialog
        pushOffersDialog = new ProgressDialogCreator(mContext, getString(R.string.offers_progressDialog_title), "", false, ProgressDialog.STYLE_SPINNER);

    }

    public int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, mContext.getResources().getDisplayMetrics());
    }

    View.OnClickListener syncButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (offerIndexesToSync.size() <= 0) {
                Functions.toast(mContext, getString(R.string.offers_text_selectOffersToSync), true);

                //reset counter;
                totalNumberOfOfferToSync = 0;
            } else {
                //set the screen active
                //keepWakeUp.acquire();

                //get initial number of offer that will be synchronized
                totalNumberOfOfferToSync = offerIndexesToSync.size();

                //show the dialog
                pushOffersDialog.showDialog();

                pushOffersToApi();
            }
        }
    };

    private void pushOffersToApi() {

        String serializedOffers = Functions.readStringFromPrefs(Constants.APP_CONTEXT,
                Constants.PREF_OFFERS_JSON, null);

        if (serializedOffers == null) {
            Functions.toast(mContext, getString(R.string.offers_text_noOffersSavedYet), true);
            return;
        }

        if (offerIndexesToSync.size() <= 0) { // SYNC FINISHED
            //turn on the screen dim
            //keepWakeUp.release();

            //close the progress dialog
            if(pushOffersDialog != null && pushOffersDialog.isShowing()) {
                pushOffersDialog.dismissDialog();
            }

            Functions.toast(mContext, getString(R.string.offers_text_selectedOffersSynchronized), true);
            onRefreshActivity();
            return;
        }

        JSInterface jsInterface = new JSInterface(mContext, OldOffersActivity.this);

        webViewHack.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                Log.e("WEBURL", url);
                if (url.indexOf("/pdf_app") != -1) { //if page loaded is not response page, skip it
                    Log.d("LOADING", "JS");
                    // run js with callback to the js interface to read response
                    webViewHack.loadUrl("javascript:HtmlViewer.showHTML" +
                            "(document.getElementsByTagName('body')[0].innerText);");
                    return;
                }
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Functions.toast(mContext, "Error " + error, true);
                pushOffersToApi();
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Functions.toast(mContext, "Server error " + errorResponse.getStatusCode(), true);
                }
                else {
                    Functions.toast(mContext, "Server error!", true);
                }

                pushOffersToApi();
                super.onReceivedHttpError(view, request, errorResponse);
            }
        });
        webViewHack.getSettings().setJavaScriptEnabled(true);
        webViewHack.addJavascriptInterface(jsInterface, "HtmlViewer");

        offers = mGson.fromJson(serializedOffers, Offer[].class);

        crtOffer = offers[offerIndexesToSync.get(0)];
        removeIfThere(offerIndexesToSync.get(0));

        //count the offers
        counterOfferSynchronized++;

        updateProgressDialogStatus(counterOfferSynchronized);

        webViewHack.loadUrl(crtOffer.pumpPdfPath + "&storePDF=true");
        Log.d("LOADING URL", crtOffer.pumpPdfPath + "&storePDF=true");
    }

    //Update progress dialog
    private void updateProgressDialogStatus(int offerDone) {
        StringBuilder message = new StringBuilder();

        message.append(String.format(getString(R.string.offers_progressDialog_messageStatus),
                String.valueOf(offerDone),
                String.valueOf(totalNumberOfOfferToSync)));
        message.append("\n\n");
        message.append(getString(R.string.offers_progressDialog_messageLoading));

        pushOffersDialog.setMessage(message.toString());
    }

    @Override
    public void onRefreshActivity() {
        try {
            Intent refresh = new Intent(mContext, OldOffersActivity.class);
            startActivity(refresh);
            finish();
        } catch (Throwable e) {

        }
    }

    @Override
    public void onPostTaskCompleted(String response) {
        //Functions.toast(mContext, "POST DONE : " + response, false);
        Log.d("POST DONE", response);
    }


    @Override
    public void OnJSONPDFResponse(int resp) {
        Log.d("RESP", "" + resp);
        crtOffer.pdfJsonID = String.valueOf(resp);
        new PostAsyncTask(OldOffersActivity.this).execute(crtOffer);
        crtOffer.synced = true;
        String newSerializedOffers = mGson.toJson(offers);
        Functions.writeStringToPrefs(Constants.APP_CONTEXT, Constants.PREF_OFFERS_JSON,
                newSerializedOffers);
        webViewHack.post(new Runnable() {
            @Override
            public void run() {
                pushOffersToApi();
            }
        });
    }
}

class JSInterface {
    private Context mContext;
    private String mHtml = "";

    private final JSInterface.OnJSONPDFResponse onJSONPDFResponse;

    JSInterface(Context ctx, JSInterface.OnJSONPDFResponse onJSONPDFResponse) {
        mContext = ctx;
        this.onJSONPDFResponse = onJSONPDFResponse;
    }

    public int getJSONResp() {
        try {
            JSONObject jsonObject = new JSONObject(mHtml);
            onJSONPDFResponse.OnJSONPDFResponse(jsonObject.getInt("response"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @JavascriptInterface
    public void showHTML(String html) {
        mHtml = html;
        getJSONResp();
    }

    public interface OnJSONPDFResponse {
        void OnJSONPDFResponse(int resp);
    }
}