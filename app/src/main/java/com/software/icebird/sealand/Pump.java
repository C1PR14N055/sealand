package com.software.icebird.sealand;

import android.widget.ImageView;

/**
 * Created by catalin on 01/09/2016.
 */
public class Pump {

    // View variables
    private String mImageFilePath;
    private ImageView mPumpImage;
    private String mPumpName;
    private String mPumpCode;
    private int mPumpId;

    //Database generated variables
    public boolean hasPerformanceValues = false;
    public int id = -1;
    public double power = -1;
    public double efficiency = -1;
    public double npsh = -1;
    public double foundX = -1;
    public double foundY = -1;
    public double errorMax = 0;
    public double errorMin = 0;
    public double searchX = 0;

    //generated variables
    public String chartUrl;
    public String pumpPdfPath;

    //performance search vars
    public double x;
    public double y;
    public String xUnit;
    public String yUnit;

    public Pump(String pumpName, String imagePath) {
        mImageFilePath = imagePath;
        mPumpName = pumpName;
    }

    public Pump() {}


    public String getPumpName() {
        return mPumpName;
    }

    public void setPumpName(String pumpName) {
        this.mPumpName = pumpName;
    }

    public ImageView getPumpImage() {
        return mPumpImage;
    }

    public String getImageFilePath() {
        return mImageFilePath;
    }

    public void setPumpImage(ImageView pumpImage) {
        this.mPumpImage = pumpImage;
    }

    public void setPumpImagePath(String path) { this.mImageFilePath = path; }

}
