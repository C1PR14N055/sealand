package com.software.icebird.sealand.adapters.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.software.icebird.sealand.R;
import com.software.icebird.sealand.interfaces.OnClickListenerRecyclerView;

/**
 * Created by Catalin on 15.09.2016.
 */
public class PumpViewWithTextViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mPumpImageView;
    public TextView mPumpNameTextView;
    public ProgressBar mPumpLoadingImage;

    private OnClickListenerRecyclerView mListenerRecyclerView;

    public PumpViewWithTextViewHolder(View itemView, OnClickListenerRecyclerView listenerRecyclerView) {
        super(itemView);

        mListenerRecyclerView = listenerRecyclerView;

        mPumpImageView = (ImageView) itemView.findViewById(R.id.imageView_pumpLayout_imagePump);
        mPumpNameTextView = (TextView) itemView.findViewById(R.id.textView_pumpLayout_namePump);
        mPumpLoadingImage = (ProgressBar) itemView.findViewById(R.id.progressBar_pumpLayout_loadingImage);

        itemView.setOnClickListener(this);
    }

    public void setPumpImage(ImageView pumpImage) {
        mPumpImageView = pumpImage;
    }

    public void setPumpName(String pumpName) {
        mPumpNameTextView.setText(pumpName);
    }

    @Override
    public void onClick(View view) {
        mListenerRecyclerView.OnClickItem(view, getLayoutPosition());
    }
}