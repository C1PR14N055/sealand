package com.software.icebird.sealand;

import android.app.Application;
import android.database.Cursor;
import android.test.ApplicationTestCase;

import com.software.icebird.sealand.utils.Functions;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
        query();
    }


    public void query () {
        Cursor cursor = Functions.execSQLGetCursor("SELECT * FROM Pump WHERE Title LIKE \"DVX 200 M\"" +
                " AND 9.08 BETWEEN CAST(AppRangeMin AS FLOAT) AND CAST(AppRangeMax AS FLOAT);");

        for(int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            System.out.println("Name: " + cursor.getString(cursor.getColumnIndex("Title")));
        }

    }

}