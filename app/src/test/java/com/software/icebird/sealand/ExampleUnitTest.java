package com.software.icebird.sealand;


import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
//ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,     ClassName TEXT,    Created TEXT,           LastEdited TEXT,            FirstName TEXT,     Surname TEXT,       Email TEXT,                 Password TEXT,                                                      PasswordEncryption TEXT,        Salt TEXT,      Company TEXT,   VAT TEXT,   Address TEXT,   State TEXT,     Province TEXT,  Commune TEXT,   Telephone TEXT,     Fax TEXT,   CAP TEXT,   NeedsApproval INTEGER
//56,                                                "Member",          "2016-03-11 13:59:16",  "2016-06-06 14:51:33",      "Alin",             "Lup",              "alin.lup@welltech.ro",     "$2y$10$032d889a15a949684a2ffuv6b55tRX2lM.f6eTFnnwmkw5gKy4N2y",     "10$032d889a15a949684a2ff1",    "blowfish",     "test",         "2332",     "test",         "test",         "test",         "test",         "2134351",          "3213213",  "test",     0
public class ExampleUnitTest {


//    String plainTextPass = "demo123";
//    String method = "$2a$";
//    String salt = "10$032d889a15a949684a2ff1";
//    String hash = "$2y$10$032d889a15a949684a2ffuv6b55tRX2lM.f6eTFnnwmkw5gKy4N2y";
//
//
//
//    // PUMP 1601 EQ
//    // SEARCH PERF: 50, ALL, 40, 50, m, l/min
//    String eq = "1,51608391608392E+1\t-4,13960113960114E-1\t1,13960113960116E-3\t3,16555872111422E-5";

//
//    @Test
//    public void addition_isCorrect() throws Exception {
//        assertEquals(4, 2 + 2);
//    }
//
//    @Test
//    public void test() {
//        String hashed = BCrypt.hashpw(plainTextPass, method + salt);
//
//        System.out.println(hashed.substring(3));
//        System.out.println(hash.substring(3));
//
//        assertEquals(hash.substring(3), hashed.substring(3));
//    }
//
//
//    @Test
//    public void solve() {
//        double flow = 10.91;
////        System.out.println("Y:" + solveY(flow, eq, "m"));
//        System.out.println("NANO: " + System.currentTimeMillis());
//    }

//    private double solveY(double x, String strEquation, String conversionRate) {
//        if (strEquation == null) { return -1; }
//        strEquation = strEquation.replace(",", ".");
//        List<Double> listEquation = new ArrayList<>();
//        for (String eq : strEquation.split("\t")) {
//            listEquation.add(Double.parseDouble(eq));
//        }
//        double y = 0;
//        for (int i = 0; i < listEquation.size(); i++) {
//            y += Math.pow(x, i) * listEquation.get(i);
//        }
//        if (conversionRate != null) { y = convertHead(y, conversionRate); }
//        return y;
//    }

    public static double convertHead(double head, String convertToUnit) {
        switch (convertToUnit) {
            case "m": {
                //Divided by 1
                break;
            }
            case "ft": {
                head /= 3.280839;
                break;
            }
            case "yd": {
                head /= 1.093613;
                break;
            }
            case "fath": {
                head /= 0.546806;
                break;
            }
        }
        head = (double) Math.round(head * 100) / 100; // 1 digit after .
        return head;
    }

    public static double convertFlow(double flow, String convertToUnit) {
        switch (convertToUnit) {
            case "l/min": {
                flow /= 16.666667;
                break;
            }
            case "l/s": {
                flow /= 0.277778;
                break;
            }
            case "m3/h": {
                // Divided by 1
                break;
            }
            case "US g.p.m.": {
                flow /= 4.402868;
                break;
            }
            case "Imp g.p.m.": {
                flow /= 3.666154;
                break;
            }
            case "ft3/min": {
                flow /= 0.588578;
                break;
            }
            case "m3/min": {
                flow /= 0.016667;
                break;
            }
            case "ft3/h": {
                flow /= 35.314667;
                break;
            }
        }
        flow = (double) Math.round(flow * 10) / 10;
        return flow;
    }

    @Test
    public void convTest(){
        System.out.println("----------FLOW-----------");
        System.out.println(convertFlow(1, "l/min"));
        System.out.println(convertFlow(1, "l/s"));
        System.out.println(convertFlow(1, "m3/h"));
        System.out.println(convertFlow(1, "US g.p.m."));
        System.out.println(convertFlow(1, "Imp g.p.m."));
        System.out.println(convertFlow(1, "ft3/min"));
        System.out.println(convertFlow(1, "m3/min"));
        System.out.println(convertFlow(1, "ft3/h"));
        System.out.println("-----------HEAD----------");
        System.out.println(convertHead(1, "m"));
        System.out.println(convertHead(1, "ft"));
        System.out.println(convertHead(1, "yd"));
        System.out.println(convertHead(1, "fath"));
    }

}